// const fetchResult = fetch("https://dog.ceo/api/breeds/image/random");
//
// fetchResult
//     .then(rsp => rsp.json())      /**** получили ответ от сервера */
//     .then(({message:src}) => {                  /**** получили тело ответ от сервера */
//         console.log({message:src})            /**** разобрали тело ответа */
//         // const {message: src} = body          /**** деструктуризируем тело ответа от сервера */
//         document.body.insertAdjacentHTML('beforeend', `<img src="${src}" alt="picture">`)
//     })



fetch('https://jsonplaceholder.typicode.com/comments', {
    method : 'POST',
    body : JSON.stringify({
        name : 'Sergej',
        age : 35,
        sex : 'man',
        position : 'student',
    }),
    headers : {
        'content-type': 'application/json'
    }


})
    .then(response => response.json())
    .then(json => console.log(json))