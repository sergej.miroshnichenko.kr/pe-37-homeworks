/**
 * Написати функцію getWeather, яка приймає ім'я міста та
 * повертає проміс, який через 1 секунду зарезолвиться,
 * якщо ми передали Kyiv, Lviv, Kharkiv,
 * інакше буде помилка з текстом 'Невідоме місто'
 *
 */

const dataBase = ['Kyiv', 'Lviv', 'Kharkiv'];
function getWeather(nameCity) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if(dataBase.includes(nameCity)){
                resolve(nameCity)
            } else{
                reject (new Error('City error'))
            }
        }, 1000)
    });
}

getWeather('Kharkiv')
    .then((result) => console.log(result))
    .catch((error) => console.log('Invalid city'))