/**
 * Доповнити функцію authenticate, яка буде резолвити успішно
 * проміс, якщо передали username: 'admin', password: '123', та
 * повертати помилку 'Invalid data' в іншому випадку
 */

const userDatabase = [{userName : 'Max', password : '123'}, {userName : 'Admin', password : '12324'}];

const authenticate = (username, password) => {
    return new Promise((resolve, reject) => {
       setTimeout(() => {
           const findUser = userDatabase.find((currentUser) => {
               return currentUser.userName === username
           })
           if (findUser === undefined){
               reject (new Error('User not found'))
           }
           if (findUser.password === password){
               resolve (findUser)
           } else{
               reject (new Error('Password incorrect'))
           }
       }, 2000);
    });
};



authenticate('Max', '123')
.then((findUser) => console.log(findUser))
.catch((error) => console.log('Invalid data'))


/**** then(findUser) можно по-другому написать find(data)  */