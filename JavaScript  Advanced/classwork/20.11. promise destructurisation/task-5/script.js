// присвоїти в змінні назву продукту, модель та ціну та
// вивести в консоль значення кожної створеної змінної

const product = {
    name: "iPhone",
    model: "12 Pro",
    company: "Apple",
    price: 1400,
};


let {
    name,
    model,
    company,
    price
} = product

console.log(name, model, company, price)