/**
 * productsPromise робить запит на сервер і іноді з'являється помилка
 *
 * Якщо продукти приходять, то вивести їх списком на сторінку,
 * інакше вивести модальне вікно з ім'ям помилки (alert)
 *
 */

// const productsPromise = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         if (Math.random() < 0.5) {
//             resolve(["orange", "apple", "pineapple", "melon"]);
//         } else {
//             reject(new Error("Server Error"));
//         }
//     }, 2000);
// });
//
// productsPromise.then((result) => {
//     if (Array.isArray(result)){
//         const listTemplate = result.reduce((acc, el, index) => {
//             if(index === 0) acc += '<ul>'
//             acc += `<li> ${el}</li>`
//             if (index === result.length -1) acc += '</ul>'
//             return acc
//         }, '')
//             return document.querySelector('.loader').innerHTML = listTemplate
//         }
//     })
//     .catch(err => alert(err.name))


// console.log("Запрос данных ...");
//
//
// const req = new Promise((resolve, reject) => {
//     setTimeout(() => {
//         console.log("Подготовка данных...");
//
//         const product = {
//             name: "TV",
//             price: 4200
//         };
//         resolve(product) /* вызываем resolve(product) если сервер дал ответ*/
//     }, 2000)
// })
//
// req.then((product) => {   /* обрабатываем положительный Promise, тоесть берем данные полученные из сервера и обрабатывам*/
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             product.status = 'order';
//             resolve(product)
//         }, 1000)
//     })
//         .then(product => {
//             console.log(product);         /* отрисовка модального окна еще 1 then закрытие */
//         })
//         .catch(() => {
//             console.error('Произошла ошибка')        /* оошибка если что то пошло не так */
//         })
//         .finally(()=> {    /* очищение формы передачи данных, мы её обязаны в любом случае очистиить */
//
//         })
// })


const test = function (time) {
    return new Promise(resolve => setTimeout(() => resolve(), time)
    )
}

Promise.race([test(1000),test(2000)]).then(() => {
    console.log('All')
})

// test(1000).then(() => {
//     console.log('1000 ms')
// });
//
// test(2000).then(() => {
//     console.log('2000 ms')
// })


const p = new Promise((resolve, reject) =>
    setTimeout(() => {
        reject("Test");
    })
);

p.then((d) => console.log("Success", d))
    .finally(() => console.log("Finally"))
    .catch((d) => console.log("Error", d))
    .then((d) => console.log("Success 2", d));

console.log(p);