//** # Завдання
// Написати функціонал логіну на сайт
// * при натисканні на кнопку `Login` перевіряються введені дані
// * через 1 секунду, якщо дані валідні, то виводиться модальне вікно з текстом Success
// * інакше в блок помилок на сторінці виводиться повідомлення _Invalid data_

const allowedUser = [{userName: 'petro', password : 'petro12'},
    {userName: 'ivan', password : 'ivan13'},
    {userName: 'stepan', password : 'stepan135'},
    {userName: 'sergej', password : 'sergej1378'}
]

function verifyUser(user, password){
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const foundUser = allowedUser.find((currentUser) => {return currentUser.userName === user})
            if (foundUser && foundUser.password === password){
                resolve(foundUser)
            }
            reject(new Error('_Invalid Data_'))
        }, 1000)
    })
}

const loginBTN = document.querySelector('#btn');
loginBTN.addEventListener('click', (event) => {
    event.preventDefault()
    let password = document.querySelector('#password').value,
        username = document.querySelector('#username').value,
        error = document.querySelector('.errors')
    error.innerText = ''

    if (password && username){
        document.getElementById('username').value = ''
        document.getElementById('password').value = ''
        verifyUser(username, password)
            .then(() => alert('Succes!'))
            .catch((err) => error.innerText = err.message)
    }
    else {
        error.innerText = 'Username and password are both required'
    }
})