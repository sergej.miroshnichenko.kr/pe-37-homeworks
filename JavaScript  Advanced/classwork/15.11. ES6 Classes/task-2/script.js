/**
 * Написати клас Modal, за допомогою якого створити 2 об'єкта
 * модальних вікон: loginModal та signUpModal
 *
 * * loginModal має наступні параметри:
 * id - 'login-modal'
 * text - 'Ви успішно увійшли'
 * classList - 'modal login-modal'
 *
 * signUpModal має наступні параметри:
 * id - 'sign-up-modal'
 * text - 'Реєстрація'
 * classList - 'modal sign-up-modal'
 *
 * Кожне модальне вікно обов'язково має наступні методи:
 * - render() - генерує html код модального вікна
 * - open() - показує модальне вікно
 * - close() - закриває модальне вікно
 *
 * - За допомогою методу redner() додати html код
 * модальних вікок в кінець body
 * - При натисканні на кнопку Login за допомогою методу openModal
 * відкривати модальне вікно loginModal
 * - При натисканні на кнопку Sign Up за допомогою методу openModal
 * відкривати модальне вікно signUpModal
 *
 */

class Modal {
    constructor(id, text, className) {
        this.id = id;
        this.text = text;
        this.className = className;
    }
    render() {
        document.body.insertAdjacentHTML('beforeend', `
    <div class="modal ${this.className}" id="${this.id}">
    <div class="modal-content">
        ${this.text}
        <span class="close">
            X
        </span>
    </div>
   </div>
    `)
        document.querySelector(`#${this.id} .close`).addEventListener('click', () => this.close())
        document.querySelector(`#${this.id} .close`).addEventListener('click', (event) =>
            // if(event.target) TODO
            this.close())
    }
    open(){
        const modalEl = document.getElementById(this.id)
        if(!modalEl){
            return console.error('Modal not find');
        }
        modalEl.classList.add('active');
    }
    close(){
        const modalEl = document.getElementById(this.id)
        if(!modalEl){
            return console.error('Cant close modal');
        }
        modalEl.classList.remove('active');
    }
}




const testModal = new Modal('testId', 'testText', 'testClassName')
const loginModal = new Modal('login-modal', 'Ви успішно увійшли', 'modal login-modal')
const signUpModal = new Modal('sign-up-modal', 'Реєстрація', 'modal sign-up-modal')



testModal.render()
loginModal.render()
signUpModal.render()

document.getElementById('login-btn').addEventListener('click', ()=> loginModal.open())
document.getElementById('sign-up-btn').addEventListener('click', ()=> signUpModal.open())
