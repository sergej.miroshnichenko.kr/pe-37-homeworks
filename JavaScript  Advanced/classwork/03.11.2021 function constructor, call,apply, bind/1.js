// 'use strict'
//
// function showName(lastName,sex,education){
//     console.log(this)
//     console.log(this.name, this.age + sex + education)
// }
//
// const user = {
//     name : 'Viktoria',
//     age : 32
// }
//
// showName.call(user,'Smith','women','highly');
// showName.apply(user,['Smith','women','school']);

// function add(c, d) {
//     return this.a + this.b + c + d;
// }
// add(10,20);
//
// let obj = {a: 1, b: 3};
//
// const mat = add.call(obj, 10,20)
//
// console.log(mat);

// function learn(){
//     console.log(this)
// }
//
// const person = {
//     name : 'Sergej',
//     age : 35,
//     sayPrivet : learn,
//     logInfo : function (job, phone){
//         console.log(`Name is ${this.name}`)
//         console.log(`Age is ${this.age}`)
//         console.log(`Job is ${job}`)
//         console.log(`Phone is ${phone}`)
//     }
// }
//
// const lena = {
//     name : 'Lena',
//     age : 28
// }
//
// person.logInfo.bind(lena, 'frontend', '380964770574')();  /* нужно вызывать ф-цию в конце () */
// person.logInfo.call(lena, 'frontend', '380964770574');  /* cразу вызывается БЕЗ () в конце */
// person.logInfo.apply(lena, ['frontend', '380964770574']);




/**
 * Наявний функціонал для друку оцінок одного студента
 * Але зі збільшенням кількості студентів постало питання про
 * розширення його
 * Для цього необхідно створити функцію-конструктор Student,
 * яка буде створювати об'єкт студента та мати ті ж самі методи
 *
 * - створити за допомогою функції-конструктора ще 2х студентів
 * - вивести оцінки кожного за допомогою метода printGrades
 * - вивести середній бал кожного студента
 * - додати метод, який буде виводити оцінку по заданій технологій
 * наприклад getGrade('html') повинен виводити оцінку студента по html
 * - вивести в консоль оцінку по js першого студента та по python - третього
 *
 *
 * ADVANCED:
 * - створити окремо функцію getStudentWithHighestResults, яка буде виводити
 * ім'я та прізвище студента за найвищим середнім балом
 */

// const student = {
//     firstName: "Margie",
//     lastName: "Sullivan",
//     sex: "female",
//     grades: {
//         html: 90,
//         css: 60,
//         js: 50,
//         python: 45,
//     },
//     printGrades() {
//         for (let key in this.grades) {
//             if (this.sex === "male") {
//                 console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
//             } else {
//                 console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
//             }
//         }
//     },
//     average: function () {
//         let sum = 0;
//         let subjectsCount = 0;
//         for (const subject in this.grades) {
//             sum += this.grades[subject];
//             subjectsCount++;
//         }
//         return sum / subjectsCount;
//     },
// };
//
// const SUBJECTS = ['html', 'css', 'python', 'js'];
//
// function Student(firstName, lastName, sex, grades) {
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.sex = sex;
//     // this.grades = grades;
//     if (grades !== null && typeof grades === 'object') {
//        this.grades = Object.keys(grades).reduce((acc,key) =>{
//            if(SUBJECTS.contains(key)){
//                let value = +grades[key]
//                acc[key] = grades[key]
//                // acc[key] = (isNaN(value)) || value < 0)
//                // ? 0
//                // : (value > 100)
//
//            } return acc
//        })
//         this.grades = grades
//     } else {
//         this.grades = {};
//         console.error('Input for grades is incorrect')
//     }
//     this.printGrades = function () {
//         for (let key in this.grades) {
//             if (this.sex === "male") {
//                 console.log(`По ${key} ${this.firstName} отримав ${this.grades[key]}`);
//             } else {
//                 console.log(`По ${key} ${this.firstName} отримала ${this.grades[key]}`);
//             }
//         }
//     }
//     this.average = function () {
//         let sum = 0;
//         let subjectsCount = 0;
//         for (const subject in this.grades) {
//             sum += this.grades[subject];
//             subjectsCount++;l
//         }
//         return sum / subjectsCount;
//     }
// }
//
// const allStudents = [
//     testStudent1,
//     testStudent2,
//     testStudent3
// ];
// function getStudentWithHighestResult(student) {
//     const bestStudent = students.reduce((best, student) => {
//         if (best.score < student.average()) {
//             best.score = student.average();
//             best.student = student;
//         }
//         return best;
//     }, {score: 0, student: null})
// }
// console.log(getStudentWithHighestResult(allStudents));
//
//
// const testStudent1 = new Student('Sergej', 'Miroshnichenko', 'male', {html: 1000, css: true, js: 150, python: 145});
// const testStudent2 = new Student('Sergej', 'Miroshnichenko', 'male', {html: 50, css: true, js: 520, python: 405});
// const testStudent3 = new Student('Sergej', 'Miroshnichenko', 'male', {html: 850, css: true, js: 50, python: 45});
//
//
//

// (function (){
//     console.log(1);
//     setTimeout(function() {
//         console.log(2);
//     },1000);
//     setTimeout(function(){
//         console.log(3);
//     },0);
//     console.log(4);
//
// })();


// const Frederik = {
//     name: 'John',
//     sales: 10,
//     sell: function(thing) {
//         this.sales += 1;
//         return 'Manager ' + this.name + ' sold ' + thing;
//     }
// };
//
// console.log(Frederik.name)
// console.log(Frederik.sales)
// console.log(Frederik.sell('Banana'))
// console.log(Frederik.sales)


// function User(name, age, human){
//     this.name = name;
//     this.age = age;
//     this.human = true;
//     this.greet = function (){
//         console.log(`Hello ${this.name}`)
//     }
// }
//
// User.prototype.entry = function (){
//     console.log(`People ${this.name} entry`)
// }
//
// const Sergej = new User('Sergej', 35);
// const Petro = new User('Petro', 32);
//
// Petro.entry();
// Petro.greet();
// Sergej.greet();

