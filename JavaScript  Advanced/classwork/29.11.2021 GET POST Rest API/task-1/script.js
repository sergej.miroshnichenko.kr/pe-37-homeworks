/**
 * Додати функціонал пошуку інформації про користувачів за ID,
    використовуючи наявне АПІ https://ajax.test-danit.com/api-pages/jsonplaceholder.html
 Технічні вимоги

 робити запит для отримання інформації тільки по одному користувачу
 відображати ім'я у вигляді текстового блоку, а електронну пошту та вебсайт у вигляді посилання
 **/

const BASE_URL = 'https://ajax.test-danit.com/api/json'
const findUser = function (id) {
    return fetch(`${BASE_URL}/users/${id}`, { method: 'GET' })
        .then(response => {
            if (response.ok) {
                return response.json()
            }
            throw new Error(response.status)
        })
        .catch(err => { throw new Error(err) })
}
const renderUser = function (container) {
    let container = container

    return function (user) {
        if (!(container instanceof HTMLElement)) return

        const { name, email, website } = user
        container.innerHTML = `
    <p>
        Name : ${name} <br>
        Mail : <a href='mailto:${email}'>${email}</a> <br>
        Website : <a href='${website}'>${website}</a>
    </p>
    `
    }
}

const containerElement = document.querySelector('.user')
const submitBtn = document.querySelector('.form-inline [type="submit"]')

function findUserHandler(event) {
    event.preventDefault()

    const dataInput = document.querySelector('.form-inline [type="text"]')
    if (!dataInput.value) {
        alert('Please enter id')
        return
    }

    findUser(dataInput.value)
        .then(renderUser(containerElement))
        // .then( userData => renderUser(containerElement)(user))
        // .then((data) => {...})
        // .then(userFn(user))
        // .then(user => {
        //     const userFn = renderUser(containerElement)
        //     userFn(user)
        // })
        .catch(err => alert(err.message))
}

submitBtn.addEventListener('click', findUserHandler)

