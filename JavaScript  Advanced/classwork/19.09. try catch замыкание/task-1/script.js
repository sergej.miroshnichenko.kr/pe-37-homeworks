/**
 * Рахувати кількість натискань на пробіл, ентер,
 * шифт та альт клавіші
 * Відображати результат на сторінці
 *
 * Створити функцію, яка приймає тільки
 * назву клавіші, натискання якої потрібно рахувати,
 * а сам лічильник знаходиться в замиканні цієї функції
 * id елемента, куди відображати результат має назву
 * "KEY-counter"
 *
 */

// function createKeyCounter(keyName){
//     let count = 0;
//     return () => ++count;
// }
//
// const enter = createKeyCounter('enter');
// const shift = createKeyCounter('shift');
// const ctrl = createKeyCounter('ctrl');
//
// const enterSpan = document.getElementById('enter-counter');
// const shiftSpan = document.getElementById('shift-counter');
// const spaceSpan = document.getElementById('space-counter');
//
//
// document.addEventListener('keydown', function (e){
//     console.log(e);
//     switch (e.key){
//         case 'Enter' :
//             enterSpan.innerText = enter();
//
//             break;
//         case 'Shift':
//             shiftSpan.innerText = shift();
//
//             break;
//         case ' ':
//             spaceSpan.innerText = ctrl()
//
//             break;
//     }
// })








