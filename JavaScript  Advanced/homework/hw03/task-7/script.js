/**   Дополните код так, чтоб он был рабочим   **/

// const array = ['value', () => 'showValue'];
//
// // Допишите ваш код здесь
// let [value, showValue] = array;
//
// alert(value); // должно быть выведено 'value'
// alert(showValue());  // должно быть выведено 'showValue'



const student = {
    name: 'John Doe',
    age: 16,
    scores: {
        maths: 74,
        english: 63
    },
    color : {
      green : 70,
      black : 50
    }
};

const {name, scores: {maths,science = 50}, color : {blue = 26, black = 71, green, yellow = 63}} = student

console.log(`${name} scored ${maths} in Maths and ${science} in Elementary Science, color blue: ${blue} in black: ${black} in green: ${green} off yellow: ${yellow}`)