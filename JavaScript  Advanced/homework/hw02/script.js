const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];


const block = document.createElement('div'),
    listItems = document.createElement('ul');


document.body.prepend(block);
block.prepend(listItems);
block.id = 'root';


const filtered = books.filter(item => item.name && item.author && item.price);

filtered.forEach(elem => {
    const obj = elem;
    const listLink = document.createElement('li');
    listLink.style.paddingBottom = '20px'
    listItems.prepend(listLink);
    listLink.innerText = `author: "${obj.author}", \n name: "${obj.name}", \n price: ${obj.price}`
})

function showBooks(books) {
    books.forEach((item, index) => {
        try {
            if (item.author === undefined) {
                throw new Error(`item ${index + 1} author: не найден `)
            } else if (item.name === undefined) {
                throw new Error(`item ${index + 1} name: не найден`)
            } else if (item.price === undefined) {
                throw new Error(`item ${index + 1} price: не найден`)
            }
            // else {
            //     listItems.insertAdjacentHTML('beforeend',`<li>author: ${item.author}, name: ${item.name}, price: ${item.price}</li>`)
            // }
        } catch (error) {
            console.log(error)
        }
    })
}

showBooks(books)

/**    Приведите пару примеров, когда уместно использовать в коде конструкцию try...catch.  **/

 // конструкция try...catch используется когда нам нужно , чтобы программа сработала даже , если будет баг.
// В таком случае выдаст пользователю текст и номер ошибки. Типичный пример - когда не подгружается база данных
// на удаленом сервере. Тоесть клиенту должен показываться спинер, прелоадер загрузки и после этого выпасть Error с номером и текстом ошибки.
// Сервер понял запрос, но не нашёл соответствующего ресурса по указанному URL. --- 404 ошибка.


