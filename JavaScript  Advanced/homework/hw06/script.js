const btn = document.getElementById('btn');

btn.addEventListener('click', () => {
    render()
})

async function getIp() {

        const response = await fetch('https://api.ipify.org/?format=json');
        const dataIP = await response.json();
        // console.log(dataIP);
        // console.log(dataIP.ip)
        return dataIP.ip;
}

async function getAddress() {
    const userIP = await getIp()
    // console.log(userIP)
    const response = await fetch(`http://ip-api.com/json/${userIP}?fields=continent,country,regionName,city,district,zip`);
    const dataAddress = await response.json();
    // console.log(dataAddress);
    return dataAddress;
}

async function render() {
    const userInfo = await getAddress();
    btn.insertAdjacentHTML('afterend', `<li class="list-text">${Object.values(userInfo)}</li>`);
    console.log(userInfo);
}






