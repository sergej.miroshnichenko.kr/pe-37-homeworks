class Employee{
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get employeeName(){
        return this.name;
    }
    set employeeName(value){
        return this.name = value;
    }
    get employeeAge(){
        return this.age;
    }
    set employeeAge(value){
        return this.age = value;
    }
    get employeeSalary(){
        return this.salary;
    }
    set employeeSalary(value){
        return this.salary = value;
    }
}

class Programmer extends Employee{

  constructor(name, age, salary, lang) {

      super(name, age, salary);
      this.lang = lang;
  }
  get salaryInfo(){
      return this.salary * 3;
  }
}

const model = new Programmer('Tesla', 2, 80000, 'English');
console.log(model);
console.log(model.salaryInfo);
const student = new Programmer('Sergej', 35, 1000, 'Ukraine');
console.log(student);

/** Прототипное наследование - это один из китов на которых строится ООП в Javascript, кроме него есть ещё инкапсуляция и полиморфизм.
    Прототипное наследование - необходимо для создания экземпляров из функции-конструктора, тоесть идёт перезаписывание Класса и в новый экземпляр дописываются нужные дополнительные параметры **/