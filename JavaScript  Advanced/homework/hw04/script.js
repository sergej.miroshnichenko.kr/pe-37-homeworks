const block = document.getElementById('root');

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => {
        if (response.ok) {
            return response.json()       /* получили массив после ответа сервера  */
        }
        throw new Error('I can not find the data')
    })
    // .then(json => console.log(json))
    .then(data => data.forEach(item => {
        // console.log(item);
        const { episodeId, name, openingCrawl, characters } = item
        // console.log(item.characters)
        renderFilms(episodeId, name, openingCrawl);
        getCharacters(characters).then(data => renderCharacters(data, episodeId))     /* обрабатываем Промис, массив персонажей */
    }))
    .catch((err) => {
        throw new Error(err)
    })

function  renderFilms(episodeId, name, openingCrawl){
    // console.log(episodeId, name, openingCrawl)
    block.insertAdjacentHTML('beforeend', `
<div class="cards" id='${episodeId}'>
<h3 class="cards-title">Title : ${name}<h3/>
<p class="cards-info">episode_id : ${episodeId}</p>
openingCrawl : <p class="cards-text"> ${openingCrawl}</p></div>`)
}


function getCharacters(characters) {
    return new Promise((resolve) => {
        const arr = [];
        characters.forEach((item) => {               /* перебор ссылок Ajax-запросы на персонажей */
            // console.log(item)
            fetch(item)                /* отправляем запрос на ссылку отдельного персонажа */
                .then(response => response.json())        /* парсинг уже отдельно полученного персонажа  */
                .then(data => {                /* полученый ... add в массив */
                    arr.push(data.name)
                    if(characters.length === arr.length){         /* если все ссылки отработали и равны изначальному массиву */
                        resolve(arr)    /* заворачиваем в resolve  */
                    }
                })
        })
    })
}

function renderCharacters(data,episodeId){
  const id = document.getElementById(episodeId);         /* id карты */
    const ul = document.createElement('ul');
    data.forEach(item => {
        ul.insertAdjacentHTML('beforeend', `<li class="list-item">${item}</li>`);
    })
    id.append(ul);
}







