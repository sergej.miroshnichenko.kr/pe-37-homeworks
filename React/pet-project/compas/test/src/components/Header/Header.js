import styles from './Header.module.scss';
import {NavLink} from 'react-router-dom';
import {ReactComponent as Logout} from "../../assets/sign-out-alt.svg";


const Header = ({isLoadedIn, setIsLoggedIn, userName}) => {

    const handleLogOut = () => {
        setIsLoggedIn(false)
    }

    return (
        <header>
            {
                isLoadedIn &&
                <nav className={styles.list}>
                    Welcome, &nbsp; <strong>{userName}</strong> &nbsp;
                    <NavLink
                        className={styles.logout}
                        onClick={handleLogOut}
                        to="/">Logout
                        <Logout/>
                    </NavLink>
                </nav>
            }
        </header>
    )
};

export default Header