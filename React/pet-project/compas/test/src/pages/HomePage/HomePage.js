import React, {useState} from "react";
import styles from './HomePage.module.scss';
import {useNavigate} from "react-router-dom";
import {useForm} from "react-hook-form";


const HomePage = ({setIsLoggedIn, setUserName}) => {
    const navigate = useNavigate();
    const {
        register,
        formState: {
            errors, isValid
        },
        handleSubmit,
        reset,
    } = useForm({
        mode: 'onChange'
    });

    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const handleChangeLogin = (e) => {
        setLogin(e.target.value);
        console.log(e.target.value)
    }

    const handleChangePassword = (e) => {
        setPassword(e.target.value);
    }

    const onSubmit = () => {
        setUserName(login);
        setIsLoggedIn(true);
        navigate('./email');
        reset();
    }
    return (
        <section className='background'>
            <form className={styles.authorisation} onSubmit={handleSubmit(onSubmit)}>
                <div className={styles.input}>
                    <input
                        {...register('firstName', {
                            required: 'please enter your name',
                            minLength: {
                                value: 3,
                                message: 'at least 3 english letters'
                            },
                            maxLength: {
                                value: 9,
                                message: 'not more 9 english letters'
                            },
                            pattern: {
                                value: /^[a-zA-Z]+$/g,
                                message: 'only english letters'
                            }
                        })}
                        className={styles.placeholder}
                        type="text"
                        placeholder='Enter your name...'
                        onChange={handleChangeLogin}
                    />

                    <input
                        {...register('lastName', {
                            required: 'please enter your password',
                            minLength: {
                                value: 3,
                                message: 'at least 3 english letters'
                            },
                            maxLength: {
                                value: 9,
                                message: 'not more 9 english letters'
                            },
                            pattern: {
                                value: /^[a-zA-Z]+$/g,
                                message: 'only english letters'
                            }
                        })}
                        className={styles.placeholder}
                        type="text"
                        placeholder='Enter your password...'
                        onChange={handleChangePassword}
                    />
                    <p className={styles.errorFirst}>{errors?.firstName && errors?.firstName?.message}</p>
                    <p className={styles.errorLast}>{errors?.lastName && errors?.lastName?.message}</p>
                </div>
                <button className={styles.submit}
                        type="submit"
                        disabled={!isValid}>Submit
                </button>
            </form>
        </section>
    )
}

export default HomePage;