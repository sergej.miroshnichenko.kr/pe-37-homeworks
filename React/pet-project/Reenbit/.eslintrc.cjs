module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "eslint:recommended",
        "plugin:react/recommended",
        "plugin:@typescript-eslint/recommended"
    ],
    "overrides": [],
    "parser": "@typescript-eslint/parser",
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "@typescript-eslint",
        "prettier"
    ],
    "rules": {
        "react/react-in-jsx-scope": "off",
        "quotes": ["error", "single"],
        "prefer-const": "warn",
        "@typescript-eslint/semi": [
            "off"
        ],
        "prettier/prettier": [
            "warn",
            {
                "semi": false,
                "singleQuote": true,
                "jsxSingleQuote": true,
                "bracketSpacing": true,
                "jsxBracketSameLine": false,
                "arrowParens": "avoid",
                "useTabs": true,
                "tabWidth": 2,
                "printWidth": 80,
                "trailingComma": "all",
            }
        ],
    }
}
