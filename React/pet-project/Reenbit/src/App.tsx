import './App.css'
import CharacterList from 'components/CharactersList/CharactersList'
import { useCharacters } from 'hooks/useCharacters'

function App() {
	const { characters, error, loading } = useCharacters()

	return (
		<div className='App'>
			<CharacterList characters={characters} />
		</div>
	)
}

export default App
