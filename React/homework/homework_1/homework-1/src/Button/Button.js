

const Button = ({className, style, text, onClick, id}) => {
    return (
        <>
            <button className={className} style={style} onClick={onClick} id={id}>{text}  </button>
        </>
    )
}


export default Button