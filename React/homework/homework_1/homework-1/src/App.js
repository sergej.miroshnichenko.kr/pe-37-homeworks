import './App.css';
import Button from './Button/Button';
import Modal from './Modal/Modal'
import React, {useState} from 'react';


function App() {

    const [modal, setModal] = useState(false);
    const [paramModal, setParamModal] = useState(null);
    const triggerModal = (e) => {

        if(e.target.id === 'firstButton'){
            setParamModal('openFist')  // записываем параметр для того чтобы понять какая кнопка
            setModal({modal: true});
        } else if
            (e.target.id === 'secondButton'){
            setParamModal('openSecond')
            setModal({modal: true});
        }
    }

    const closeModal = (event) => {
        // if(event.target.classList.contains('background') ){
            setModal(false);
            setParamModal(null);
        // }
    }


    return (
        <div className="App">

            <Button id='firstButton' text='Open first modal' style={{backgroundColor: 'yellow'}} onClick={triggerModal}/>
            <Button id='secondButton' text='Open second modal' style={{backgroundColor: 'green'}} onClick={triggerModal}/>

            {modal && <Modal closeModal={closeModal} header={ paramModal === 'openFist'? 'modal 1' : 'modal 2'}
                             status={true} text={ paramModal === 'openFist'? 'This is first modal' : 'This is second modal' }
                             actions={
                <>
                    <Button onClick={closeModal} className='btnModal' text='Ok' style={{backgroundColor: 'green'}}/>
                    <Button onClick={closeModal} className='btnModal' text='Cancel' style={{backgroundColor: 'red'}}/>
                </>
            }/>}
        </div>
    );
}

export default App;
