import React from 'react'
import './Modal.css'
import Button from '../Button/Button'

const Modal = ({header, text, status, actions, closeModal}) => {
    return (
        <div className='background'>
            {status && <Button className='buttonX' id={'closeButton'} onClick={closeModal} text="x"/>}

            <div className='modalBlock'>
                <div className='content'>

                    {header}
                    {text}

                    <div className='button'>
                        {actions}
                    </div>
                </div>
            </div>
        </div>
    )
}


export default Modal;