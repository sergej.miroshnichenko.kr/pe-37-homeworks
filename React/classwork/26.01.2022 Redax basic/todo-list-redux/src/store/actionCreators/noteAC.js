import { ADD_NOTE, TOGGLE_IS_DONE } from '../actions/notesAction'

export const addNotes = (value) => ({ type: ADD_NOTE, payload: value });
export const toggleIsDone = (id) => ({ type: TOGGLE_IS_DONE, payload: id})