import React, {useState} from 'react';
import styles from './AddNote.module.scss'
import {useDispatch} from "react-redux";
import {addNotes} from '../../store/actionCreators/noteAC';
import {Button, TextField} from '@mui/material';


const AddNote = () => {
    const dispatch = useDispatch();
    const [value, setValue] = useState('');

    return (
        <>
            <div className={styles.root}>
                <TextField
                    value={value}
                    variant="outlined"
                    onChange={(e) => {
                        setValue(e.target.value)
                    }}
                    type='text'
                    label='Your note'
                    className={styles.input}
                />
                <Button onClick={() => {
                    dispatch(addNotes(value));
                    setValue('');
                }} className={styles.btn} type="button" variant="contained">Add Note</Button>
            </div>
            <div className={styles.line}/>
        </>
    )
}

export default AddNote

