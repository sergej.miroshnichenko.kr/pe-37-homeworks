import React from 'react'
import NoteItem from '../NodeItem/NodeItem';
import { useSelector } from 'react-redux';


const NoteContainer = () => {

    const notes = useSelector(state => state.notes);

    if (!notes) return <p style={{ fontSize: 24, marginLeft:20 }}>You don't have any notes yet</p>;

    return (
        <ul>
            {notes && notes.map(({ text, isDone, id }, index) => <NoteItem id={id} isDone={isDone} index={index + 1} text={text} key={text} />)}
        </ul>
    )
}

export default NoteContainer