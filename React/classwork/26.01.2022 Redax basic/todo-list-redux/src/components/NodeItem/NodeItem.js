import React from "react";
import classNames from "classnames";
import styles from "./NoteItem.module.scss";
import { Button, Checkbox } from "@mui/material";
import { toggleIsDone } from '../../store/actionCreators/noteAC';
import { useDispatch } from 'react-redux'


const NodeItem = ({ index, text, id, isDone }) => {
    const dispatch = useDispatch();

    return (
        <li className={classNames(styles.root, { [styles.rootDone]: isDone })}>
            <div className={styles.wrapper}>
                <Checkbox onChange={() => {
                    dispatch(toggleIsDone(id))
                }} className={styles.checkbox}/>
                <span>{index}.</span>
                <p className={classNames({ [styles.done]: isDone })}>{text}</p>
            </div>

            <div className={styles.wrapper}>
                <Button className={styles.btn} variant="contained">
                    Add
                </Button>

                <Button color="error" variant="contained" className={styles.btn}>
                    Delete
                </Button>
            </div>
        </li>
    );
};

export default NodeItem;
