import React from 'react';
import {Route, Routes} from "react-router-dom";
import CounterPage from "./pages/CounterPage/CounterPage";


const AppRoutes = () => (
    <Routes>
        <Route path='/counter' element={<CounterPage/>} />
    </Routes>
);

Routes.propTypes = {};
Routes.defaultProps = {};

export default AppRoutes;
