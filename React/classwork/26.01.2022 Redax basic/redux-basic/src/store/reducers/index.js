import {INCREMENT, DECREMENT, SET_COUNTER, CLEAR_COUNTER, NEW_TITLE, NEW_FACT, IS_LOADING} from "../actions/counterActions";

const initialState = {
    counter: 0,
    title: 'Some title',
    data: '',
    isLoading: false,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case INCREMENT : {
            return {...state, counter: state.counter + 1}
        }
        case DECREMENT : {
            if(state.counter > 0) {
                return {...state, counter: state.counter - 1}
            }
            return state
        }
        case CLEAR_COUNTER : {
            return {...state, counter: 0}
        }
        case SET_COUNTER : {
            if (action.payload && typeof action.payload === 'number') {
                return {...state, counter: action.payload}
            }
            return state
        }
        case NEW_TITLE : {
            return {...state, title: 'title'}
        }
        case NEW_FACT : {
            return {...state, data: action.payload}
        }
        case IS_LOADING : {
            return {...state, isLoading: action.payload}
        }
        default : {
            return state;
        }
    }
}

export default reducer
