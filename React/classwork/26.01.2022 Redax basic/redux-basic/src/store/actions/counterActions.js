export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';
export const CLEAR_COUNTER = 'CLEAR_COUNTER';
export const SET_COUNTER = 'SET_COUNTER';
export const NEW_TITLE = 'NEW_TITLE';
export const NEW_FACT = 'NEW_FACT';
export const IS_LOADING = 'IS_LOADING';
