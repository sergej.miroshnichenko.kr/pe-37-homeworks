import {
    CLEAR_COUNTER,
    DECREMENT,
    INCREMENT,
    SET_COUNTER,
    NEW_TITLE,
    NEW_FACT,
    IS_LOADING
} from "../actions/counterActions";

export const incrementCounterAC = () => ({ type: INCREMENT })
export const decrementCounterAC = () => ({ type: DECREMENT })
export const clearCounterAC = () => ({ type: CLEAR_COUNTER })
export const titleAC = () => ({ type: NEW_TITLE })
export const setCounterAC = (counterValue) => ({ type: SET_COUNTER, payload: counterValue })
export const isLoadingAC = (value) => ({type: IS_LOADING, payload: value})






export const getFactsAC = () => async (dispatch) => {
    dispatch(isLoadingAC(true))
    const data = await fetch('https://catfact.ninja/fact').then(res => res.json())

    dispatch(isLoadingAC(false))
    dispatch({type: NEW_FACT, payload: data})
}