import './App.css';
import Header from "./components/Header/Header";
import {BrowserRouter as Router} from "react-router-dom";
import AppRoutes from "./AppRoutes";
import {Provider} from "react-redux";
import store from "./store";


function App() {

    return (
        <Provider store={store}>
            <Router>
                <div className="App">
                    <Header/>
                    <section>
                        <AppRoutes/>
                    </section>
                </div>
            </Router>
        </Provider>
    );
}

export default App;
