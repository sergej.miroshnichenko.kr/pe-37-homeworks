import React, {useState} from 'react';
import styles from './Counter.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {
    clearCounterAC,
    decrementCounterAC,
    incrementCounterAC,
    setCounterAC, titleAC
} from "../../store/actionCreators/counterActionCreators";



const Counter = () => {
    const counter = useSelector(({counter}) => counter);
    const dispatch = useDispatch();
    const [value, setValue] = useState('')

    const incrementCounter = () => {
        dispatch(incrementCounterAC())
    }
    const decrementCounter = () => {
        dispatch(decrementCounterAC())
    }
    const clearCounter = () => {
        dispatch(clearCounterAC())
    }
    const setCounter = () => {
        dispatch(setCounterAC(+value));
        setValue('')
    }
    return (
        <div className={styles.root}>
            <p className={styles.counter}>{counter}</p>
            <div className={styles.btnWrapper}>
                <button onClick={incrementCounter}>+</button>
                <button onClick={decrementCounter}>-</button>
                <button onClick={clearCounter}>Clear counter</button>
                <button onClick={() => {
                    dispatch(titleAC())
                }}>change T</button>
            </div>

            <div className={styles.btnWrapper}>
                <input value={value} onChange={(event) => setValue(event.target.value)} type="text"/>
                <button onClick={setCounter}>Set value</button>
            </div>
        </div>
    )
}

export default Counter;
