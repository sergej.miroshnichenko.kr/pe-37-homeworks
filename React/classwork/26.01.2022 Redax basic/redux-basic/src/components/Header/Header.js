import React from 'react';
import styles from './Header.module.scss';
import { NavLink } from "react-router-dom";
import {useSelector} from 'react-redux'


const Header= () => {

    const title = useSelector(({title}) => title)

    return (
        <header className={styles.root}>
           <span>{title}</span>
            <nav>
                <ul>
                    <li>
                        <NavLink activeClassName={styles.active} to="/counter">Counter</NavLink>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;
