import React from 'react';
import Counter from "../../components/Counter/Counter";
import {getFactsAC} from '../../store/actionCreators/counterActionCreators'
import {useDispatch, useSelector} from "react-redux";




const CounterPage = () => {
    const dispatch = useDispatch();
    const fact = useSelector(({fact}) => fact);
    const isLoading = useSelector(({isLoading}) => isLoading);


    if(isLoading){
        return (
            <>
                <h1>Loading...</h1>
            </>
        )
    }

    return (
        <>
            <p>{fact}</p>
            <button onClick={() => dispatch(getFactsAC())}>Get facts</button>
            <h1>Counter</h1>
            <Counter/>
        </>
    )
};


export default CounterPage;
