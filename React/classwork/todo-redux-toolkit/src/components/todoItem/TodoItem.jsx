import React from 'react';
import styles from './TodoItem.module.scss'
import { removeNote, toggleCompleted } from "../../store/todoSlice";
import { useDispatch } from "react-redux";


const TodoItem = ({ id, completed, value }) => {

    const dispatch = useDispatch()

    return (
        <>
            <li key={id} className={styles.todoItem}>
                <input className='qwerty' onChange={() => dispatch(toggleCompleted({ id }))}
                       type="checkbox"
                       checked={completed}
                />
                <span>&nbsp;{value}</span>
                <span className={styles.itemDel} onClick={() => dispatch(removeNote({ id }))}>&nbsp; &#10008;</span>
            </li>
        </>
    );
};

export default TodoItem;