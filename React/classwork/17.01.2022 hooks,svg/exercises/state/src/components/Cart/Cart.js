import React from 'react';
import styles from './Cart.scss';
import CartItem from "../CartItem";


const Cart = (props) => {

    const {items} = props;


    return (
        <section className={styles.root}>

            <h2>CART</h2>
            <div className={styles.items}>
                {items && items.map(item => <CartItem {...item} />)}     {/* CartItem - отдельная карточка добавленная в корзину */}
            </div>

            <p>{items.length > 0 ? 'Total: ' + items.reduce((acc, item) => (acc += item.price * item.count, acc), 0) + '$' : 'Price : 0' + '$'}</p>

            {/*  вариант # 2:   <p>{ 'Price: ' + items.reduce((acc, item) => {*/}
            {/*    acc += item.price * item.count;*/}
            {/*    return acc;*/}
            {/*}, 0) +'$' } </p>*/}

        </section>
    )
}

export default Cart;