import React from 'react';
import styles from './Item.module.scss';
import { ReactComponent as StarIcon } from "../../assets/svg/star-plus.svg";
import { ReactComponent as StarRemove } from "../../assets/svg/star-remove.svg";

const Item = (props) => {
    const { name, price, image, addToCart } = props;

    return (
        <div className={styles.root}>
            <div className={styles.favourites}>
                < StarIcon />
                < StarRemove />
            </div>
            <p>{ name }</p>
            <img src={image} alt={name} />
            <span>{price}$</span>
            <button onClick={() => addToCart(name, price)}>Add to cart</button>
        </div>
    )
}

export default Item;