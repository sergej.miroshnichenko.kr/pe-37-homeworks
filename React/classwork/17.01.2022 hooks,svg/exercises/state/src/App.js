import React, {useEffect, useState} from "react";
import ItemsContainer from "./components/ItemsContainer";
import Cart from "./components/Cart";
import styles from './App.module.scss';


const itemsFruits = [
    {
        name: 'Orange',
        price: 300,
        image: 'https://cdn.shopify.com/s/files/1/0409/2562/6532/products/online-gulayan-makati-orange-1pc-17411997663396_grande.jpg?v=1594059033',
    },
    {
        name: 'Lime',
        price: 500,
        image: 'https://static.libertyprim.com/files/familles/lime-large.jpg?1569491474',
    },
    {
        name: 'Lemon',
        price: 400,
        image: 'https://www.collinsdictionary.com/images/full/lemon_234304936.jpg',
    }
];


function App() {

    const [items, setItems] = useState(itemsFruits);       /* все карточки товаров */
    const [cartItem, setCartItem] = useState([]);       /* карточки товаров попавшие в корзину */

     useEffect(() => {
         ( async () => {
             const res = await fetch('http://localhost:3001/items')
                 .then(res  => res.json())
             setItems(res.data);
         }) ()
     }, [])


    const addToCart = (name, price) => {
        // console.log('name:' ,name, 'price:' ,price)
        setCartItem((prev) => {
            const index = prev.findIndex(item => item.name === name);  /* для каждого элемента массива сравниваем имя из элемента массива с именем которое пришло из callback */

            if (index === -1) {       /* если элемент не найден в массиве */
                return [...prev, {name, price, count: 1}]       /* по клику на 1-ю карточку товара в пустой корзине формируется объект (эта карточка) в котором имя,цена и количество стартует с 1-го  */
            } else {
                const newState = [...prev]      /* раскрываем старый state, копируем его в новую константу и мутируем его  */
                newState[index].count += 1      /* index - номер элемента */
                return newState;
            }
        })
    }


    return (
        <div className={styles.root}>
            <div>
                <ItemsContainer items={items} addToCart={addToCart}/> {/* все карточки товаров */}
            </div>

            <div>
                <Cart items={cartItem}/>     {/* корзина */}
            </div>
        </div>
    );
}

export default App;
