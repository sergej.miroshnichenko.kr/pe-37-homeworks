import React, {useEffect, useState} from 'react';
import {Switch, Route, Redirect, useLocation} from 'react-router-dom';
import HomePage from "../pages/HomePage";
import PostsPage from "../pages/PostsPage/PostsPage";
import PostPage from "../pages/PostPage/PostPage";
import NoMatchPage from "../pages/NoMatchPage/NoMatchPage";
import SignIn from "../pages/SignIn/SignIn";

const Routes = ({ setIsAuth, isAuth }) => {
    // const { pathname } = useLocation();
    // const [isLogin ,setIsLogin] = useState(localStorage.getItem('isLogin'))
    //
    // useEffect(() => {
    //     setIsLogin(localStorage.getItem('isLogin'));
    // }, [pathname])


    // console.log('pathname', pathname)
    // console.log('isLogin', isLogin)

    return (
        <Switch>

            <Route exact path='/'>
                <HomePage/>
                {isAuth ? <HomePage/> : <Redirect to="/sign-in"/> }
            </Route>
            <Route exact path='/posts'>
                <PostsPage/>
            </Route>
            <Route exact path='/authors'>
                <h1>Authors heading</h1>
            </Route>
            <Route exact path='/sign-in'>
                <SignIn setIsAuth={setIsAuth}/>
            </Route>
            <Route exact path='/posts/:id' component={PostPage}/>
            <Route path="*">
                <NoMatchPage/>
            </Route>

        </Switch>
    )
}
export default Routes