import React, {useEffect, useState} from "react";
import axios from "axios";
import PostsContainer from "../../components/PostsContainer";



const PostPage= (props) =>{
    const {match,history} = props;
console.log(props)

    const [postData, setPostData] = useState(null)

    const getPost = async () =>{
        try {
            const { status, data } =await axios.get(`https://ajax.test-danit.com/api/json/posts/${match.params.id}`)
            if (status === 200){
              setPostData(data)
          }

        }
        catch (error){
            console.error(error)
        }

    }


    useEffect(()=> getPost(), [])

    if (!postData){
        return(
            <section>

                <h1>Something went wrong!</h1>
                <button onClick={() => history.goBack()}>GO BACK</button>
            </section>

        )
    }
    return(
        <section>
            <p>{postData.id}</p>
            <h1>{postData.title}</h1>
            <p>{postData.body}</p>
        </section>
    )
}

export default PostPage;