import React, {useEffect, useState} from "react";
import axios from "axios";
import PostsContainer from "../../components/PostsContainer";


const PostsPage= (props) =>{

    const { history } = props;

    const [postsData, setPostsData] = useState([])

    const getPosts = async () =>{
        try {
            const { status, data } = await axios.get('https://ajax.test-danit.com/api/json/posts')

          if (status === 200){
              setPostsData(data)
          }

        }
        catch (error){
            console.error(error)
        }
    }


    useEffect(() => getPosts(), [])

    return(
        <section>

            <h1>Posts</h1>
            <PostsContainer history={history} posts={postsData}/>
        </section>
    )
}

export default PostsPage;