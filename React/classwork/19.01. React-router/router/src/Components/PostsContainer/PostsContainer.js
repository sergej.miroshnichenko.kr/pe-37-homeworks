import React from 'react';
import Post from "../Post";
import styles from './PostsContainer.module.scss';
import Preloader from "../Preloader";

const PostsContainer = (props) => {
    const {posts} = props;

    return (
        <section className={styles.root}>
            <div className={styles.postsContainer}>
                {posts.map(({id, ...args}) => <Post key={id} id={id} {...args} />)}
            </div>
        </section>
    );
};

export default PostsContainer;
