import React from 'react';
import styles from './Navigation.module.scss';

const Navigation= (props) => {
    const { title, user: { name, age, avatar } } = props;

    return (
        <header className={styles.root}>
            <span>{title}</span>
            <div className={styles.userContainer}>
                <img src={avatar} alt={`user ${name}`} />
                <span>{name}, {age}</span>
            </div>
        </header>
    );
};

export default Navigation;
