import React from 'react';
import PropTypes from "prop-types";
import styles from './Header.module.scss';
import {NavLink} from "react-router-dom";

const Header= (props) => {
        const { title, user: { name, company } } = props;

        return (
              <header className={styles.root}>
                  <span>{title}</span>
                  <nav>
                      <ul>
                          <li>
                              <NavLink exact to="/">Home</NavLink>
                          </li>
                          <li>
                              <NavLink exact to="/posts">Posts</NavLink>
                          </li>
                          <li>
                              <NavLink exact to="/authors">Authors</NavLink>
                          </li>
                          <li>
                              <NavLink exact to="/sign-in">Sign in</NavLink>
                          </li>
                      </ul>
                  </nav>
                  <div className={styles.userContainer}>
                      <span>{name}, {company}</span>
                  </div>
              </header>
        );
};

export default Header;
