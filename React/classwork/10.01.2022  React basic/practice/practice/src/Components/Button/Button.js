import styles from "./Button.module.scss";
import React, {useState} from "react";


const Button = () => {

    const [counter, setCounter] = useState(0)
    return(
        <div>
            <p className={styles.para}>{ counter }</p>
            <div className={styles.btn}>
                <button onClick={ () => setCounter (prev => prev + 1 ) }>Увеличить</button>
                <button onClick={ () => setCounter( prev => prev - 1 ) }>Уменьшить</button>
                <button onClick={ () => setCounter( Math.random() ) }>Рандом</button>
                <button onClick={ () => setCounter(0) } className={styles.btnElement}>Обнулить</button>
            </div>

        </div>
    )
}

export default Button


