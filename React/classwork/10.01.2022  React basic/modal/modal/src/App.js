import React from "react";
import './App.scss'

class App extends React.Component {

    state = {
        isOpen: false,
    }


    openModal = () => {
        this.setState({isOpen: true});
    }

    closeModal = () => {
        this.setState({isOpen: false});
    }

    render() {

        const {isOpen} = this.state;

        return (
            <>
                {isOpen && <div className="modal">
                    <div onClick={this.closeModal} className="background"/>
                    <div className="content">
                        <button onClick={this.closeModal}>X</button>
                        <h1>MODAL1</h1>

                        <button>YES</button>
                        <button onClick={this.closeModal}>NO</button>
                    </div>
                </div>}

                <button onClick={this.openModal}>OPEN FIRST MODAL</button>

            </>
        );
    }
}

export default App;
