import React, {Component} from 'react'
import './App.css';

class App extends Component {

    state = {
        title: 'Some Title',
        user: {
            name: 'Sergej',
            age: 35,
            hobby: 'football'
        }
    };

    incrementAge = () => {
        this.setState((current) => ({       // current - это копия всего state c 6 по 13-ю строку
            user: {
                ...current.user,     // полная копия user cо всеми свойствами
                age : current.user.age + 1   //  конкретно в свойстве age к предыдущему значению +1
            }
        }))
    }

    updateTitle = () => {
        this.setState({
            title :'Another title'
        })
    }

    render() {
        const {title, user} = this.state;


        return (
            <div className="block">
                <h2>{title}</h2>
                <p>{user.name}</p>
                <p>{user.age}</p>
                <p>{user.hobby ? user.hobby : 'Sergej dont hobby'}</p>
                <button onClick={this.incrementAge}>Увеличить возраст</button>
                <button onClick={this.updateTitle}>Изменить заголовок</button>
            </div>
        )
    }
}


export default App;
