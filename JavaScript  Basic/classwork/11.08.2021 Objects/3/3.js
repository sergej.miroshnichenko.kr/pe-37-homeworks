/** Написать функцию которая входным параметром будет принимать обьект,
    возвращаемым значением этой функции должна быть КОПИЯ на этот обьект.
*/


function cloneObject(object){
    let emptyObject = {}
    for (let key in object){
        emptyObject[key] = object[key]
    }
    return emptyObject
}

const obj = {
    name : 123,
}

const obj2 = cloneObject(obj)
console.log(obj2 === obj)