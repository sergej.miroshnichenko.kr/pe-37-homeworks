

/** Напишите код для суммирования всех зарплат и сохраните результат в переменной sum. Должно получиться 390.

 Если объект salaries пуст, то результат должен быть 0 */


let salaries = {
    John: 500,
    Ann: 160,
    Pete: 130
}

let sum = 0;
for (let key in salaries) {
    sum = sum + salaries[key]

}
console.log(sum)


let sum = 0;

const goodsAddedToCart = [
    {
        name : 'JS',
        price : 20
    },
    {
        name : 'React',
        price : 30
    },
    {
        name : 'HTML',
        price : 202
    }
];


for (let key in goodsAddedToCart) {
    sum += goodsAddedToCart[key].price;
}
console.log(sum);


let menu = {
    width: 200,
    height: 300,
    title: "My menu"
};

function multiplyNumeric(obj) {
    for (let key in obj) {
        if (typeof obj.key === 'number') {
            return menu = obj.key * 2;
        }
    }
}

multiplyNumeric()
console.log(menu)
