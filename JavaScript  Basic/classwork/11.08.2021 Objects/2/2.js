
/** ЗАДАНИЕ - 2
* Добавить к предыдущему заданию функционал.
* В возвращаемом объекте должен быть метод, который увеличивает возраст на 1.
* Т.е. внутри объекта будет свойство\ключ\поле, значением которого будет являться функция,
* которая увеличивает свойство\ключ\поле age ЭТОГО объекта на 1
* */



let name = prompt("Введите имя пользователя");
let age = prompt("Введите возраст пользователя")

function creatNewUser (name, age){
    const person = {
        name,
        age,
        addAge : function (){
            return ++this.age
        }
    }
    return person
}
let result = creatNewUser(name, age);

console.log(result.addAge());