const obj1 = {
    key: 'value',
    key1: 'value',

    getValue(params) {
        const that = this
        // function name(params) {
        // 	console.log(that.key)
        // }
        // name()
    },
}

// function name() {
// 	this
// }

// obj1.getValue(01)

// const user = {
// 	age: 26,
// 	hobby: 'basket',
// 	name: 'vanya',
//     null: 22,
//     undefined: 'wqwe',
//     NaN: true,
//     const: 'qwe',
//     function: 'qwe',
// }

// for (let gogi of Object.entries(user)) {
// 	console.log(gogi)
// }

// const arr = []

// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then(response => response.json())
// 	.then(data => {
// 		// const newArray = json.map(item => item.title)
// 		data.forEach((currentItem, index) => {
// 			const text = document.createElement('p')
// 			text.textContent = `${++index} : ${currentItem.title}`
// 			document.body.append(text)
// 		})
// 	})

// console.log(arr.length)

// const sbmt = document.getElementById('submit')

// sbmt.addEventListener('click', (event) => {
//     event.preventDefault()
// 	const fields = document.querySelectorAll('.field')

// 	const data = {}

// 	fields.forEach(fieldElement => {
//         data[fieldElement.name] = fieldElement.value
// 	})

// 	console.log(data)
// })

/** как собрать со всех полей формы данные   */

const fields = document.querySelectorAll('.field')


const data = {}
fields.forEach(item => {
    item.addEventListener('keydown', (event) => {
        data[event.target.name] = event.target.value
    })
})
