/** TASK - 1
* Create sum() function.
* Arguments: two integer numbers
* Return value: sum of two arguments
* */


/**Создайте функцию sum().
* Аргументы: два целых числа.
* Возвращаемое значение: сумма двух аргументов*/

const sum = (a, b) => (a + b)

console.log(sum(1,2))