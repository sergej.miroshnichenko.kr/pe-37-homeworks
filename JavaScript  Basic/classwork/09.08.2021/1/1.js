/** ЗАДАНИЕ - 1
* Написать функцию суммирования.
* Принимает аргументы: первое число и второе число
* Возвращаемое функцией значение: сумма двух аргументов
* */



function calcSumm (firstNum, secondNum){
    let firstArgs = parseInt(firstNum);
    let secondArgs = parseInt(secondNum);
    if (firstNum && secondNum){
        return firstArgs+secondArgs
    }else {
        return null;
    }

}
console.log(calcSumm(1,2));