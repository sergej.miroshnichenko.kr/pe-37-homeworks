/** ЗАДАНИЕ - 4
* Написать функцию, которая из всех переданных аргументов возвращает только выбранный тип данных.
* Аргументы:
*   1 - выбранный тип данных, который нужно отоборать
*   2 и далее - елементы, из которых нужно отобрать
*
* ДОП. ЗАДАНИЕ: Написать вторую реализацию, где элементы из которых нужно отбирать переданы в массиве.
* */



function filterDataTypes(dataTypes, ...args){
    let filterArray = [];

    for (let item of args){
        if (typeof item === dataTypes){
            filterArray.push(item);
        }
    }
    return filterArray;
}
let slon = filterDataTypes('object',{},null, 5, "5");
console.log(slon);