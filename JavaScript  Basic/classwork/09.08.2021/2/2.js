/** ЗАДАНИЕ - 2
 * Написать функцию которая будет принимать два аргумента - число с которого начать отсчет и число до которого нужно досчитать.
 * Под отсчетом имеется в виду последовательный вывод чисел в консоль с увеличением на единицу.
 */



function declaration (firstNum, secondNum){
    if (firstNum <secondNum){
        let temp = firstNum;
        firstNum = secondNum;
        secondNum = temp;
    }
    for (let i = firstNum; i <= secondNum; i++){
        console.log(i);
    }

}
declaration(3,21);