/**TASK 1
* Write a function customCharAt(string,index)
*   string - source string
*   index - the index of the particular character of the string that we need
* Return value: the character itself, that is placed on the specific index
* */


/**Напишите функцию customCharAt(string,index)
* string - исходная строка
* index - индекс конкретного символа строки, который нам нужен
* Возвращаемое значение: сам символ, помещенный на определенный индекс**/


function customCharAt(string, index){
    return string[index]
}


let result = customCharAt('world',4)
console.log(result)