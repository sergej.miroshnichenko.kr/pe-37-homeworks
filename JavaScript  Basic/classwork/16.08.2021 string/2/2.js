/**TASK 2
 * Create a function. It should receive 1 argument - source string.
 *
 * Turn all of the odd characters of the string in to UPPERCASE.
 *
 * Return value: string with uppercase odd characters
 */


/**ЗАДАНИЕ 2
 * Создайте функцию. Она должна принимать 1 аргумент - исходную строку.
 *
 * Переведите все нечетные символы строки в UPPERCASE.**/



let str = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. A architecto atque dignissimos distinctio earum est facere facilis fuga, ipsa labore laborum obcaecati possimus qui ratione similique suscipit tempora temporibus totam.';
function transform ( str) {
    let newStr = "";
    for(let i = 0; i < str.length; i++) {
        if ( i % 2 === 0 ) {
            newStr += str[i].toUpperCase();
        }  else {
            newStr += str[i];

        }

    }
    return newStr;
}

let res = transform(str);
console.log(res);


// работает