/** TASK - 2
 Создайте два квадрата, используя тот же способ, который был описан в предыдущем задании.
* Но эти квадраты будут иметь разные цвета фона.
* Создайте два элемента в JS.
* Спросите пользователя о размере обоих квадратов.
* Спросите пользователя о цвете фона первого квадрата.
* Спросите пользователя о цвете фона второго.
* Добавьте стили к обоим квадратам.
* Поместите оба квадрата перед первым элементом с тегом script на странице.
*/

const createSquare = (size, color) => {
    let element = document.createElement('div')
    element.style.width = size + 'px'
    element.style.height = size + 'px'
    element.style.backgroundColor = color
    element.style.margin = '10px'
    return element
}

function createMoreSquare(count, parent) {
    let sizeUsers = parseInt(prompt('Введите размер стороны'))

    for (let i = 0; i < count; i++) {
        const generateColor = '#' + Math.random().toString(16).substr(-6)
        let square = createSquare(sizeUsers, generateColor)   // <div style = {width : size}></div>
        parent.before(square)
    }
}

let someScript = document.querySelector('script')
createMoreSquare(prompt('сколько хоч квадратов?'), someScript)