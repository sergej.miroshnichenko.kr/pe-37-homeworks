// function creatElem() {
//     let elem = document.getElementsByTagName('p')
//     for (let i = 0; i < elem.length; i++) {
//         // console.log(elem[i].parentNode)
//         console.log(elem[i].previousSibling.)
//     }
// }
//
// creatElem()

// let p = document.createElement('p')
// p.innerHTML = 'Привет барсук'
// p.classList.add('green', 'main');
// document.body.appendChild(p)
// console.log(p);
//
// let div = document.insertBefore(p,document.querySelector('#out'))
//
// console.log(div)



/** TASK - 1
 * Создайте квадрат.
 * Спросите пользователя о размере и цвете фона квадрата.
 * Создайте элемент в JS.
 * Задайте размер.
 * Задайте цвет фона.
 * Добавьте стили к элементу в JS
 * Поместите элемент ДО первого тега скрипта на вашей странице.
*/

function createSquare(size = 10, color = '#00ff00') {
    const div = document.createElement('div')
    div.style.backgroundColor = color
    div.style.width = size + 'px'
    div.style.height = size + 'px'
    return div
}

let colorUser = prompt('Enter the color', '#0000ff')
let sizeUser = parseInt(prompt('Enter the size of form', '10'))
let script = document.querySelector('script')
const square = createSquare(sizeUser, colorUser)

script.before(square)
// script.before(createSquare(colorUser, sizeUser))

