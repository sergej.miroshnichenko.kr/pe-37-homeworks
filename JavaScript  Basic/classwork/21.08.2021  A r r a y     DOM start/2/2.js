/** ЗАДАЧА - 2
* Создайте функцию, которая будет принимать один аргумент - массив из результата предыдущего задания.
* Задача состоит в том, чтобы показать в консоли каждое блюдо из массива и удалить его из исходного массива.
* После того, как все блюда будут выведены на консоль, массив должен быть пуст.
* */
/** Каждое блюдо должно быть помещено в массив.
* Возвращаемое значение: массив со всеми блюдами.
* */

const getBreakfastUser = () => {
    let breakfast = []
    let dish = prompt('Что будете на завтрак?')

    while (dish !== 'end') {
        breakfast.push(dish)
        dish = prompt('Что будете на завтрак?')
    }

    return breakfast
}

function cleanArray(srcArray) {
    const length = srcArray.length

    for (let i = 0; i < length; i++) {
        console.log(srcArray.pop())
    }

    console.log(srcArray)
}

cleanArray(getBreakfastUser())