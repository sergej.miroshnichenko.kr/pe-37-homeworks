/** ЗАДАЧА - 1
* Получите несколько элементов со страницы по:
* тег
* класс
* идентификатор
* CSS-селектор
* атрибут name
* Используйте метод console.dir() для отображения элементов.
* */


let tag = document.getElementsByTagName('h1')
let getClassElem = document.getElementsByClassName('title')
let identificator = document.getElementById('root')
let selector = document.querySelector('.title')
let attribute = document.querySelector('[name = "hello"]')

console.log(tag)
console.log(getClassElem)
console.log(identificator)
console.log(selector)
console.log(attribute)