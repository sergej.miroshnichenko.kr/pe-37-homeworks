
/** ЗАДАЧА - 3
* Найдите элемент с классом 'remove-me' и удалите его со страницы.
* Найдите элемент с классом 'make-me-bigger'. Замените класс 'make-me-bigger' на 'active'. Класс 'active' уже существует в CSS.
* */

function removeMe (){
    const elem = document.querySelector(".remove-me");
    elem.remove();
    elem.style.display = 'none'
}
removeMe()


function makeBig(){
    let elem = document.querySelector('.make-me-bigger')
    elem.className = 'active'
}
makeBig()