/** ЗАДАЧА - 2
* Создайте функцию, которая будет:
* брать элементы со страницы с классом 'training-list', и текстовым содержимым равным 'list-element 5'.
* Показывать этот элемент в консоли.
* Заменит текстовое содержимое этого элемента на "<p>Здравствуйте</p>", не создавая нового HTML-элемента на странице.
* Используйте методы массива для выполнения задачи.
* */


function changeContent(){
    const listItems = document.querySelectorAll(".training-list")
    // console.log(listItems[0].innerText())
    // console.log(listItems[0].textContent())
    // console.log(listItems[0].outerText())
    listItems.forEach(currentItem => {
        if (currentItem.textContent === 'list-element 5'){
            currentItem.innerHTML = '<p>Hello<p>'

        }
    })
}

changeContent()
