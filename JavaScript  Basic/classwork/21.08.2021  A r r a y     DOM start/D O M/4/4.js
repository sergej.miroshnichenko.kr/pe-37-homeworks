/** TASK - 4
* На экране пользователя есть список товаров.
* Создайте функцию, которая будет находить "закончившиеся" предметы. Количество элементов 'run out' равно 0.
* Замените 0 внутри текстового содержимого этих элементов на 'run out' и измените цвет текста на красный.
*/

function changeAmount(){
    let list = document.querySelectorAll(".storage-item")
    list.forEach(function (item){
       let splitString = item.textContent.split('-');
       if (splitString[1].trim() === '0'){
           let newStr = item.textContent.replace(" 0", " run out");
           item.textContent = newStr
           item.style.color = "yellow";
       }
    })
}
changeAmount()