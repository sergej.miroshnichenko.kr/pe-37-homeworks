//*** Создайте функцию, которая будет принимать массив в качестве единственного аргумента.
// возвращаемое значение: новый массив, который является точной копией исходного.
// * Задача должна быть выполнена с использованием: for, map(), оператора spread. Это означает три реализации.**//


function cloneArray(array) {
	const clonedArray = []
	for (let elemOfArray of array) {
		clonedArray.push(elemOfArray)
	}
	return clonedArray
}

console.log(cloneArray())

// **********  второй вариант решения  *************
//
// const cloneArray = array => {
// 	const newArr = array.map(function (item) {
// 		return item
// 	})
// 	return newArr
// }

// *****************   третий вариант решения *********************
// const cloneArray = array => array.map(item => item)
//
// const cloneArray = array => [...array]