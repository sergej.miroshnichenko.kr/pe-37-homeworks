/** TASK - 2
* Добавьте третий аргумент в предыдущую функцию task. Это будет число, которое описывает, сколько раз вы хотите показать сообщение.
* */

function showMsg(msgText, time, count) {
    const timer = setInterval(() => {
        console.log(msgText)
        count--;
        if (count <= 0){
            clearInterval(timer)
        }
    }, time)
}

showMsg('hello func it"s work', 1500, 4)


// или ниже вариант решения через рекурсию


//
// function showMsg(msgText,time,number){
//     setTimeout(() => {
//         if (number === 0){
//             return
//         }
//         else {
//             console.log(msgText)
//             number--;
//             showMsg(msgText,time,number)
//         }
//     },time)
// }
//
// showMsg('hello my friend', 2000, 3)