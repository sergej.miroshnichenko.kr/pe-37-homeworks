/**
 * Задача - 02
 *
 * Создайте пользовательское модальное окно.
 *
 * CSS и разметка размещены в html и css файлах этой классной работы.
 *
 * Модальное окно должно появляться после нажатия на кнопку "Показать модальное окно".
 *
 * Модальное окно должно закрываться после нажатия на кнопку "крестик" в правом верхнем углу или после нажатия в любом месте экрана, кроме самого модала.
 * */


const showModalBtn = document.createElement('button');
showModalBtn.textContent = 'Открыть модальное окно';
document.body.prepend(showModalBtn);

showModalBtn.addEventListener('click', () => {
    const modalwrapper = document.querySelector('.modal-wrapper');
    modalwrapper.style.display = 'flex';

    modalwrapper.addEventListener('click', function (event) {
        if (event.target === event.currentTarget || event.target.classList.contains('modal - close')) {
            modalwrapper.style.display = 'none';
        }
    })
})

function createModal(text) {
    const modal = document.createElement('div')
    modal.className = 'modal-wrapper'

    modal.insertAdjacentHTML(
        'afterbegin',
        `<div class="modal">
            <button class="modal-close">x</button>
            <p class="modal-text">${text}</p>
        </div>`,
    )

    modal.addEventListener('click', function (ev) {
        if (ev.target === ev.currentTarget || ev.target.classList.contains('modal-close')) {
            modal.remove()
        }
    })

    return modal
}

showModalBtn.addEventListener('click', () => {
    const modalInBody = createModal('Lorem ipsum dolor sit amet, consectetur adipisicing elit')

    document.body.prepend(modalInBody)
})




