/** TASK - 1
* Напишите функцию showMsg(msgText, time), где:
* msgText - текст сообщения, которое будет показано.
* time - количество миллисекунд задержки для показа сообщения
* */

// const array = ['hello', 'i', 'student', 'danit']
//
// let index = 0
// const showList = setInterval(() => {
//     if (index === array.length) {
//         return
//         // clearInterval(showList)
//     } else {
//         console.log(array[index])
//         index++
//     }
// }, 500)

function showMsg(msgText, time) {
    setTimeout(() => {
        console.log(msgText)
    }, time)
}

showMsg('hello func it"s work', 1500)