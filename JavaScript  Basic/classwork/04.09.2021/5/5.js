/** TASK - 3
* Проверьте, есть ли в localStorage значение, связанное с ключом 'userName'.
* Если localStorage содержит значение по ключу 'userName', покажите сообщение "Hello, userName", вместо userName поместите фактическое значение из хранилища.
* Если в localStorage нет ключа userName:
* - попросите пользователя ввести свое имя.
* - сохранить его в localStorage по ключу userName
* - вывести сообщение Hello, userName
* */


const localStorageUserName = localStorage.getItem('userName');
if (localStorageUserName !== null){
    console.log(`Hello ${localStorageUserName}`)
}else {
    const userName = prompt('Enter your Name')
    localStorage.setItem('userName', userName);
    console.log(`Hello ${userName}`)
}