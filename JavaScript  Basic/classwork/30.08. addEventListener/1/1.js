/** ЗАДАЧА - 1
* Покажите оповещение с сообщением 'This is click', после нажатия на кнопку 'Click me'.
*/

const btn = document.createElement('button');
document.body.append(btn);
btn.textContent = 'Click me'
btn.addEventListener('click', event => {
    alert('This is click')
})

