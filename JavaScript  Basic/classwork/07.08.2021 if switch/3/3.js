/** TASK - 3
* Get the access group name from the user, it can be - 'admin', 'manager' or 'user'.
* And show different message for different access group:
*   - for admin - "Hello, admin!"
*   - for manager - "Hello, manager!"
*   - for user - "Hello, user!"
* */

// Получить имя группы доступа от пользователя, это может быть - 'admin', 'manager' или 'user'.
//     И покажите разные сообщения для разных групп доступа:
//     - для admin - "Hello, admin!"
// - для менеджера - "Привет, менеджер!
// - для пользователя - "Привет, пользователь!"


let introduce = prompt("Введи имя группы доступа от пользователя, admin, manager, user");

switch (introduce) {
    case "admin":
        window.location.assign(
            'https://www.devteam.space/wp-content/uploads/2019/11/Getting-Rid-Of-Bad-Developers-During-A-Project.gif'
        );
        break;

    case "manager":
        window.location.assign(
            'https://www.devteam.space/wp-content/uploads/2019/11/Getting-Rid-Of-Bad-Developers-During-A-Project.gif'
        );
        break;

    case "user":
        window.location.assign(
            'https://www.devteam.space/wp-content/uploads/2019/11/Getting-Rid-Of-Bad-Developers-During-A-Project.gif'
        );
        break;

    default :
        document.write(`<h1> Вы не представились </h1>`)
        break;
}
