/** ЗАДАНИЕ - 2
* Вывести в консоль первые 147 непарных чисел.
*   ПРОДВИНУТАЯ СЛОЖНОСТЬ - не выводить в консоль те числа, которые делятся на 5.
* */



// for (let i = 0, number = 0; number <= 147; i++ ){
//     if (number % 2 !== 0 && number % 5 !== 0 ){
//         console.log(number);
//         number++
//     }
// }
for (let i = 0, number = 1; number <= 20; i++) {
    if (i % 2 !== 0 && i % 5 !== 0) {
        console.log(i);
        number++;
    }
}
