function toggle(){
    let toggleTheme;
    if (!localStorage.theme) localStorage = 'light'
    document.body.className = localStorage.theme
    toggleTheme = document.body.classList.contains('dark')? 'Cменить на тёмную тему' : 'Cменить на светлую тему'

    const toggleThemeBtn = document.getElementById('toggleThemeBtn')
    toggleThemeBtn.addEventListener('click', () => {
        document.body.classList.toggle('dark');
        toggleThemeBtn.innerText = document.body.classList.contains('dark')? 'Cменить на светлую тему' : 'Cменить на тёмную тему'
        localStorage.theme = document.body.className || 'light'
    })
}

toggle()



