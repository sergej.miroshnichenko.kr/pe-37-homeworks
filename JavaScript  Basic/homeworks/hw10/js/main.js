const button = document.querySelector('button'),
      input = document.querySelectorAll('input'),
      i = document.querySelectorAll('i');
button.style.marginTop = '15px';

function showPassword() {
    i.forEach((item, index) => {
        item.addEventListener("click", function () {
            if (item.classList.contains('fa-eye')) {
                item.classList.remove('fa-eye');
                item.classList.add('fa-eye-slash');
                input[index].type = "password";
            } else {
                item.classList.remove('fa-eye-slash');
                item.classList.add('fa-eye');
                input[index].type = 'text';
            }
        })
    })
}
showPassword()


function checkForMatches() {
    button.addEventListener('click', () => {

        const check = document.getElementById('eye').value,
              checkSlash = document.getElementById('eye_slash').value;

        if (check && checkSlash !== '') {
            if (check === checkSlash) {
                alert('You are welcome')
            } else {
                const span = document.querySelector('span');
                span.textContent = 'Нужно ввести одинаковые значения';
                span.style.cssText = 'color:red; font-weight:600; font-size: 12px';
            }
        } else {
            alert('You did not enter your password !');
        }
    })
}
checkForMatches()