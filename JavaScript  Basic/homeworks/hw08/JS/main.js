const input = document.createElement('input'),
      error = document.querySelector('p');

document.body.append(input);
input.insertAdjacentText('beforebegin', 'price, $');
input.type = 'number';
input.style.margin = '10px';

input.addEventListener('focus', (event) => {
        event.target.style.borderColor = 'green';
    }
)
input.addEventListener('blur', (event) => {
        function returnText() {
            if (!isNaN(input.value) && input.value > 0) {

                const priceContainer = document.createElement('div'),
                      priceElement = document.createElement('span');
                      removeButton = document.createElement('button')

                removeButton.classList.add('remove')
                removeButton.innerText = 'X'
                removeButton.addEventListener('mousedown', () => {
                        priceContainer.remove()
                    }
                )
                document.body.append(priceContainer)
                priceContainer.append(priceElement);
                priceContainer.append(removeButton)
                removeButton.style.margin = '10px'
                input.classList.add('green')
                priceElement.innerText = 'Текущая цена : ' + input.value
                input.value = ''    /** после вывода значения в span очищаем input*/
            } else {
                input.classList.add('error')
                error.prepend(input.value);
                error.innerHTML = 'Please enter correct price'
                error.style.color = 'red'
            }
        }

        returnText()
        event.target.style.borderColor = ''
    }
)


