const btns = document.querySelector('.button-wrapper'),
      pictures = document.querySelector('.images-wrapper')
      indexPicture = 0;
let timerId;
// timerId = creatInterval()

function creatInterval() {
    return setInterval(() => {
        showSlides()
    }, 1000)
}

btns.addEventListener('click', (event) => {
    if (event.target.id === 'STOP') {
        clearInterval(timerId)
        // event.target.setAttribute('disabled', true)
        // btns.children[1].setAttribute('enabled', true)

    } else {
        timerId = creatInterval()
        // event.target.setAttribute('disabled', true)
        // btns.children[0].setAttribute('enabled', true)
    }
})

function showSlides() {
    pictures.children[indexPicture].hidden = true
    indexPicture++
    if (indexPicture === pictures.children.length) {
        indexPicture = 0
    }
    pictures.children[indexPicture].hidden = false
}
showSlides()

/**#Теоретический вопрос

   1. Опишите своими словами разницу между функциями setTimeout() и setInterval().
   2. Что произойдет, если в функцию setTimeout() передать нулевую задержку? Сработает ли она мгновенно, и почему?
   3. Почему важно не забывать вызывать функцию clearInterval(), когда ранее созданный цикл запуска вам уже не нужен?

Ответы:
1. функциями setTimeout() - вызовит функцию 1 раз через определенный интервал времени.
   функция setInterval() - будет вызывать функцию регулярно через определенный интервал времени.

 2. В данном случае вызов функции произойдет сразу после завершения выполнения текущего кода. Затем она сработает мгновенно.
 3. Функцию clearInterval()отменяет многократные повторения действий, установленные вызовом функции setInterval().





 */