const tabBtns = document.querySelector('.tabs'),
    tabTitle = document.querySelectorAll('.tabs-title')
tabItem = document.querySelectorAll('.tabs-item')


tabBtns.addEventListener('click', (event) => {
        let currentBtn = event.target,
            tabId = currentBtn.getAttribute('data-tab'),
            currentText = document.querySelectorAll('.tabs-item')


        tabTitle.forEach(function (item) {
            item.classList.remove('active')
        })

        currentText.forEach(function (item) {
                let currentDataText = item.getAttribute('data-tab')
                tabId === currentDataText ? item.classList.remove('dn') : item.classList.add('dn')
            }
        )
        currentBtn.classList.add('active')
    }
)

//
//
// current - текущий элемент на котор навешен обработчик
//     event target - текущий элемент на который был сделан клик