
let num;

do{
    num = +prompt('Please, input your number');

}while (num === '' || Number.isNaN(num) || num % 1 !== 0)

function factorial (num){
    if (num > 1) {
        return num * factorial(num - 1)
    } else {
        return  1
    }
}

console.log(factorial(num))


/** Рекурсивная функция - функция, которая вызывает сама себя. Её используют, когда вычисление функции можно свести к её более простому вызову, а его – к ещё более простому и так далее, пока значение не станет очевидно **/



// return (n != 1) ? n * factorial(n - 1) : 1;
