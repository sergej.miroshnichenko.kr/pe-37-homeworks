const btns = document.querySelectorAll('.btn');

document.addEventListener('keydown', (event) => {
    btns.forEach(button => {
        if (event.key === button.innerText|| event.code === 'Key' + button.innerText){
            button.classList.add('active')
        }else{
            button.classList.remove('active');
        }
    })
})


/** 1.  Почему для работы с input не рекомендуется использовать события клавиатуры?
События клавиатуры предназначены для работы именно с клавиатурой. Иначе могут быть неточности. К примеру
 текст может быть вставлен при помощи правого клика и меню без единого нажатия клавиатуры.
 * */
