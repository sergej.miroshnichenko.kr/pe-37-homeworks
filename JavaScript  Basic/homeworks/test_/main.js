const ls = [51, 56, 58, 59, 61];
let k = 3; // количество элементов в задании
let maxSum = 174;
function getSubset(data, subset) {
    let result = [];
    for (let i = 0; i < subset.length; i++) result[i] = data[subset[i]];
    return result;
}
let subsets = [];
let temp = [];
for (let i = 0; (temp[i] = i) < k - 1; i++) {}
subsets.push(getSubset(ls, temp));
for (;;) {
    let i;
    for (i = k - 1; i >= 0 && temp[i] === ls.length - k + i; i--);
    if (i < 0) {
        break;
    }
    temp[i]++;
    for (++i; i < k; i++) {
        temp[i] = temp[i - 1] + 1;
    }
    subsets.push(getSubset(ls, temp));
}
//subsets содержит массив вариантов
let sumSubsets = [];
subsets.forEach((item, index) => {
    sumSubsets[index] = item.reduce((accumulator, currentValue) => accumulator + currentValue, 0);
});
sumSubsets.sort(function (a, b) {
    return b - a;
});
for (let i = 0; i < sumSubsets.length; i++) {
    if (sumSubsets[i] < maxSum) {
        console.log(sumSubsets[i]);
        break;
    }
}