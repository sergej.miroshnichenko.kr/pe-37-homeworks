import Link from 'next/Link';
import { useEffect } from 'react';
import { useRouter } from 'next/router';

const NotFoundPage = () => {

    const router = useRouter();

    useEffect(() => {
        setTimeout(() => {
            router.push('/');
        }, 3000)
    }, [router])

    return(
        <div className='not-found'>
            <h1>oops!</h1>
            <h2>There is no such page here!</h2>
            <p>Going to the <Link href='/'><a>home page</a></Link> 3 seconds later... </p>
        </div>
    )
}

export default NotFoundPage;