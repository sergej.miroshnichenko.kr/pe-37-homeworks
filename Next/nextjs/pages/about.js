

const AboutPage = () => {
    return (
        <div>
            <h1>About us</h1>
            <p>Greasy burgers are good old - fashioned classic recipes, the right size and at a reasonable price.</p>
            <p>They are cooked in the American, Mexican, or Italian style, sparing neither sauce nor toppings.</p>
            <p>For those who get hungry, there is the immense &quot; Fatest Burger &quot; with five beef patties, corn chips and cheese.</p>
        </div>
    )
}

export default AboutPage;