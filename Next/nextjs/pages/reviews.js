import Head from "next/head";


const Reviews = ({reviews}) => {

    return (
        <>
            <Head>
                <title>Fat Burgers | Home</title>
                <meta name='title' content='Fat Burgers'/>
            </Head>
            <div>
                <h1>Customer reviews</h1>
                <div className='title'>
                    {reviews.length !==0 && reviews.slice(0,20).map(res => {
                        return(
                            <div key={res.id} className='review'>
                                {res.id}. {' '}
                                {`${res.body.slice(0, 90)}...`}
                            </div>
                        )
                    })}
                </div>
            </div>
        </>
    )
}

export async function getServerSideProps() {
    const response = await fetch(`https://jsonplaceholder.typicode.com/comments`);
    const data = await response.json();

    return {
        props: {
            reviews: data.slice(0, 20)
        }
    }
}

export default Reviews;