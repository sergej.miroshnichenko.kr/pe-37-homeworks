import {BsGithub} from 'react-icons/Bs'

const Footer = () => {
    return (
        <footer className='footer'>
            <p>© by Sergej Miroshnichenko</p>
            <a href="https://github.com/sergejmiroshnichenko?tab=repositories"> <BsGithub/> Github </a>
            <p>2022</p>
        </footer>
    )
}

export default Footer