import { FC, useState } from 'react';
import styles from './Product.module.scss'
import { IProduct } from "../../models";


interface ProductProps {
    product: IProduct
}

export const Product: FC<ProductProps> = ({ product }) => {

    const [details, setDetails] = useState(false);

    const showDetails = () => {
        setDetails(details => !details)
    }

    const detailsValue = details ? 'Hides Details' : 'Show Details'
    const backgroundColorDetails = details ? 'burlywood' : 'yellow'

    return (
        <div className={styles.product_box}>
            <img src={product.image} style={{ width: '100px', height: '100px' }} alt='product'/>
            <p>{product.title}</p>
            <p><b>{product.price}</b></p>
            <button onClick={showDetails}
                    style={{
                        background: `${backgroundColorDetails}`,
                        padding: '10px',
                        border: 'none',
                        cursor: 'pointer'
                    }}
            >{detailsValue}</button>
            {details && <p>{product.description}</p>}
        </div>
    );
};

