import { useEffect, useState } from "react";
import { IProduct } from "../models";
import axios, { AxiosError } from "axios";

export const useProducts = () => {
    const [products, setProducts] = useState<IProduct[]>([]);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState('');

    const fetchProduct = async () => {
        try {
            setError('')
            setLoading(true)
            const res = await axios.get<IProduct[]>('https://fakestoreapi.com/products?limit=5');
            console.log('data🚀', res.data)
            setProducts(res.data)
            setLoading(false)
        } catch (e: unknown) {
            const error = e as AxiosError
            setLoading(false)
            setError(error.message)
        }
    }

    useEffect(() => {
        fetchProduct()
    }, []);

    return { products: products, loading, error }
}