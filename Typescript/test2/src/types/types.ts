
export interface IAddressProps {
    street: string;
    city: string;
}

export interface IUser {
    id: number,
    name: string,
    username: string,
    email: string,
    address: IAddressProps
}