import React, { FC } from 'react';
import { IUser } from "../types/types";


interface UserItemProps {
    user: IUser
}

export const UserItem: FC<UserItemProps> = ({ user }) => {
    return (
        <div style={{padding: '15px', border: '1px solid black'}}>
            {user.id}. {user.username} проживает в городе {user.address.city} по улице {user.address.street}
        </div>
    );
};


