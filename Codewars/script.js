/*****     Превратить в строке цыфры до 5 на "0", свыше 5 на "1"   ****/

// function fakeBin(x) {
//     let result;
//     let number = x.toString().split('');
//     for (let i = 0; i < number.length; i++) {
//         if (number[i] < 5) {
//             number[i] = '0';
//         }
//         if (number[i] >= 5) {
//             number[i] = '1';
//         }
//     }
//     result = number.join('')
//     return result;
// }
//
// fakeBin('54089');

/** Если число делиться на два остальных вернуть ТРУ, если нет False  **/

// function isDivideBy(number, a, b) {
//     if (number % a === 0 && number % b === 0) {
//         return true;
//     } else {
//         return false;
//     }
// }
//
// isDivideBy(45, 5, 9);

/**   Первая буква заглавная, остальные маленькие  **/

// function capitalizeWord(word) {
//    return word[0].toUpperCase() + word.slice(1).toLowerCase();
// }
// capitalizeWord('miroshnichenko')

/** Вернуть противоположное число **/

// function opposite(number) {
//     let res = 0;
//     if(number > 0){
//         return  Math.abs(number) * -1;
//     }
//     if(number < 0) {
//         return  Math.abs(number);
//     }
//
//     return res;
// }
// opposite(-12525220.3325)

/** Удалить все пробелы из строки и вернуть строку **/

// function noSpace(x) {
//
//    return x.split(' ').join('');
// }
//
// noSpace('8pr 8 yu 10 privet oscar');

/***  Вхождение    **/

// function solution(str, ending){
//     if (str.includes(ending[1])){
//         console.log(true)
//     } else{
//         console.log(false)
//     }
// }
// solution('samurai', 'rai')


/***  Если парное умножаем на 8, если не парное на 9   **/

// function simpleMultiplication(number) {
//     if (number % 2 === 0) {
//         return number * 8;
//     } else {
//         return  number * 9;
//     }
// }
//
// simpleMultiplication(5)


/***   повторить строку заданное кол-во раз  **/

// function repeatStr (n, s) {
//
//     return s.repeat(n)
// }
//
// repeatStr(3,'mo')

/***  Вычислить третий угол   **/

// function otherAngle(a, b) {
//
//     return  180 - (a + b);
// }
//
// otherAngle(43,78);

/***  Вычислять тип данных   **/

// function typeOfSum(a, b) {
//
//     return typeof (a + b);
// }
//
// typeOfSum(null, false);


/***       Каждый день аренды автомобиля стоит 40 долларов. Если вы арендуете автомобиль на 7 и более дней,
 * вы получаете скидку 50 долларов. Или, если вы арендуете автомобиль на 3 или более дней, вы получаете скидку $20.
 Напишите код, который выдает общую сумму за разные дни(d).      **/

// function rentalCarCost(d) {
//     if(d<3) return d * 40;
//     if(7>d && d>=3) return (d*40 -20);
//     if(d>=7) return (d*40 -50);
// }
// rentalCarCost(2)
//

/****  Озвучить заданное число ***/

// function switchItUp(number){
//     switch (number) {
//         case 1: return "One"; break;
//         case 2: return "Two"; break;
//         case 3: return "Three"; break;
//         case 4: return "Four"; break;
//         case 5: return "Five"; break;
//         case 6: return "Six"; break;
//         case 7: return "Seven"; break;
//         case 8: return "Eight"; break;
//         case 9: return "Nine"; break;
//         case 0: return "Zero"; break;
//         default:
//             return "You're not 1,2,3 or 4!";
//     }
// }
// switchItUp(2)

// 100, если оценка за экзамен больше 90 или если количество выполненных проектов больше 10.
// 90, если оценка за экзамен больше 75 и если количество выполненных проектов не менее 5.
// 75, если оценка за экзамен более 50 и если количество выполненных проектов не менее 2.
// 0, в других случаях
/****    ***/

// function finalGrade(exam, projects) {
//
//     if (exam > 90 && projects > 10) {
//         console.log(100);
//     }
//     if ((exam > 75 && exam < 90) && (projects >= 5 && projects < 10)) {
//         console.log(90);
//     }
//     if ((exam > 50 && exam < 75) && (projects >= 2 && projects < 5)) {
//         console.log(75);
//     }
//     if (exam < 50  && projects < 2) {
//         console.log(0);
//     }
// }
//
// finalGrade(85, 5);

/**************  Проверить заканчивается ли строка началом другой строки  **/

// function solution(str, ending){
//
//     if(ending === ''){
//         return true
//     }
//     let res = str.slice(-ending.length);
//     let result = res === ending ? true : false;
//     return result;
// }
//
// еще вариант решения        return str.endsWith(ending);
//
// solution('samurai', 'ra')


/****  Ближайший насос находится в 50 милях! В среднем ваш автомобиль расходует около 25 миль на галлон. Осталось 2 галлона.
 Учитывая эти факторы, напишите функцию, которая скажет вам, можно ли добраться до насоса или нет.
 Функция должна возвращать true (1 ), если это возможно, и false (0 ), если нет.     ***/

// const zeroFuel = (distanceToPump, mpg, fuelLeft) => {
//
//     return mpg * fuelLeft >= distanceToPump
//     if (zeroFuel > 1){
//         return true;
//     } else {
//         return false;
//     }
// };
// zeroFuel(50,25,2);

/***   Perimeter sequence   ****/

// function perimeterSequence(a,n) {
//
//     return a * n * 4;
//
//     }
//
// perimeterSequence(1,3);


/****  drills-traffic-light Cветофор  ****/

// function updateLight(current) {
//     if (current === 'yellow') {
//         return 'red';
//     }
//     if (current === 'green') {
//         return 'yellow';
//     }
//     if (current === 'red') {
//         return 'green';
//     }
// }
//
// updateLight('red');

/***   Student's Final Grade   **/

// function finalGrade(exam, projects) {
//
//     if(exam > 90 || projects > 10) return 100;
//     if(exam > 75 & projects >= 5) return 90;
//     if(exam > 50 & projects >= 2) return 75;
//     return 0;
// }
//
// finalGrade(85, 5);


/***  Merge two sorted arrays into one   ****/

// function mergeArrays(arr1, arr2) {
//     if(arr1 === [] && arr2 === []) return [];
//     let res;
//     res = [...arr1,...arr2].sort((a,b) => a-b);
//     let p = [...new Set(res)];
//     return p;
// }
// mergeArrays([-100, -27, -8, 5, 23, 56, 124, 325, -34, 12],[6,7]);


// function add(num1, num2) {
//     const arr1 = num1.toString().split('');
//     const arr2 = num2.toString().split('');
//     console.log(arr2[arr2.length-1]);
//     const arr = [];
//
//     arr1.reduceRight((arr1,arr2) => {
//         console.log(arr1[arr1.length-1] += arr2[arr2.length-1]);
//     })
// arr1.reduceRight(function(previousValue, currentValue, index, arr2) {
//     return previousValue + currentValue;
// },arr.length-1);

// }
// add(122, 81);

/******     Add property to every object in array  (Добавить свойство в каждый объект массива)       *****/

// let questions = [{
//     question: "What's the currency of the USA?",
//     choices: ["US dollar", "Ruble", "Horses", "Gold"],
//     corAnswer: 0
// }, {
//     question: "Where was the American Declaration of Independence signed?",
//     choices: ["Philadelphia", "At the bottom", "Frankie's Pub", "China"],
//     corAnswer: 0
// }];

// Решение верное
// questions.forEach((item) => {
//     item.usersAnswer = null;
// })
//
// console.log(questions)


// questions.map(obj => ({ ...obj, usersAnswer: null }));
// res = questions.map(obj => ({ ...obj, usersAnswer: null }));
// for (let i = 0; i < questions.length; i++) {
//
//     questions[i].usersAnswer = null;
// }
// let [{question: question, choices: choices, corAnswer: corAnswer, usersAnswer = null}] = questions;
//
// console.log([{question,choices,corAnswer,usersAnswer},{question,choices,corAnswer,usersAnswer}])


// questions.map(({question, choices, corAnswer}) => ({question,choices,corAnswer, usersAnswer: null}));

/***   Find the missing element between two arrays (Найти не повторяющиеся числа из массивов)   ****/

// function findMissing(arr1, arr2) {
//     arr1.sort()
//     arr2.sort()
//     for (let i = 0; i < arr1.length; i++) {
//         if(arr1[i] !== arr2[i]){
//             return arr1[i]
//         }
//     }
// }
// findMissing([6, 1, 3, 6, 8, 2], [3, 6, 6, 1, 2])

// решение №2

// function findMissing(arr1, arr2) {
//     const reducer = (accumulator, currentValue) => accumulator + currentValue
//     return arr1.reduce(reducer, 0) - arr2.reduce(reducer, 0);
// }

// function combine() {

//     const objA = { a: 10, b: 20, c: 30 };
//     const objB = { a: 3, c: 6, d: 3 };
//     const objNew = {...objA,...objB};
//     console.log(objNew)
// }


// arr = [2,5,6,1];
// arr2 = [10,10,10]
//
//
//  let res = arr.reduceRight((accum,value) => accum + value)
// console.log(res)
// let result = arr.reduceRight((acc, elem) => {
//     console.log(acc)
// return acc+=elem;
// })
// console.log(result);

/****   Maximum Triplet Sum (Array Series #7)    Найти сумму трёх наибольших чисел в массиве без дублей     ******/
//
//  function maxTriSum(numbers) {
//
//     const res = numbers.sort((a, b) => b - a)
//
//     return ([...new Set(res)].slice(0, 3).reduce((acc, value) => acc + value))
// }
//
// maxTriSum([8,6,4,8,3,9,4,2,6,4,2,3,2,0,1]);


/***********Find the missing element between two arrays  Найти во 2-м массиве 1 число которого нет в 1-м массиве  ************ */


// function findMissing(arr1, arr2) {
// Решение №1
//  if (arr2.length === 0)  return arr1[0];
//  for (let i = 0; i < arr1.length; i++) {
//      for (let j = 0; j < arr2.length; j++) {
//          if (arr1[i] === arr2[j]) {
//               let p = arr1.splice(i,1)[0];
//             let k = arr2.splice(j,1)[0];
//             i--
//             j--
//          }
//      }
//  }
// console.log(arr1[0]);
//
// Решение №2
//     if(arr2.length === 0) return arr1[0]
//     let a = arr1.reduce((acc, val) => acc + val);
//     let b = arr2.reduce((acc, val) => acc + val);
//     return a - b;
//
//     Решение №3
//
//     arr1 = arr1.sort();
//     arr2 = arr2.sort();
//     for(let i = 0; i < arr1.length; i++){
//         if(arr1[i] != arr2[i]) return arr1[i];
//     }
// }
//
// findMissing([6, 1, 3, 6, 8, 2], [3, 6, 6, 1, 2]);

/****  Sum with Arrows  Сумма двух массивов ****/

// var Sum = 'sum = (function(arr){ return arr.reduce((a,b) => { return a + b}); })';

/****  Unpacking Arguments     Распаковать аргументы при помощи спред оператора ****/

// function spread(func, args) {
//     return func(...args);
// }


// function vowelOne(s) {
//
//     let letter = ['a', 'e', 'i', 'u', 'o', 'y'];
//     let result = s.split('');
//     for (let i = 0; i < result.length; i++) {
//         for (let j = 0; j < letter.length; j++) {
//             if (result[i] === letter[i]) {
//                return  result[i] = 1
//             }console.log(result)
//         }
//     }
// }
//
// vowelOne('vowelOne')

/***************  Data Reverse    Разделить большой массив на 8 частей и поменять их местами    ****/

// function dataReverse(data) {
//
//  let arr = []
//     for (let i = 0; i < data.length; i=i+8) {
//
//         arr.unshift(...data.slice(i, i+8))
//     }
//     console.log(arr);
// }
//
// dataReverse([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16])
// выйдет  dataReverse([9,10,11,12,13,14,15,16, 1,2,3,4,5,6,7,8])

// решение 2
//     [...data.join(``).match(/.{8}|$/g).reverse().join(``)].map(Number);


// function add(num1, num2) {
//
//     let digits = Math.max(num1.length,num2.length);
//     const arr1 = num1.toString().split('').reverse();
//     const arr2 = num2.toString().split('').reverse();
//
//     let arr3 = [];
//
//
//     for (let i = 0; i < digits.length; i++) {
//         arr3.push((arr1[i] || 0) + (arr2[i] || 0));
//     }
//
//     console.log(+(arr3.reverse().join('')));


//     for(let i = 0; i < arr1.length; i++){
//         console.log(arr2[0] = arr2[0]);
//         sum.push(arr1[i] + arr2[i]);
//
//
//     }
// console.log(sum)
// let sum = arr1.map((num, index) => {
//     return num + arr2[index]
// })
// console.log(sum)


// }
// add(122, 81);


/******  Преобразовать массив чисел в символы  ******/
// let ArrowFunc = function(arr) {
//     return arr.map( x => String.fromCharCode(x) ).join('');
// }


// function myLanguages(results) {
//     const arr = [
//         {"Java": 10, "Ruby": 80, "Python": 65},
//         {"Hindi": 60, "Dutch": 93, "Greek": 71},
//         {"C++": 50, "ASM": 10, "Haskell": 20}]
//
//     arr.forEach(item => {
//         return Object.entries(item).filter(([key, value]) => value > 60)
//     })
// }
//
// myLanguages()

/**********   Отфильтровать объект по значению и отсортировать на уменьшение    *******/

// function myLanguages(results) {
//
//   let p = Object.entries(results).filter(([key, value]) => value >= 60);
//   return [].concat(...p).filter(item => typeof (item) === 'string').sort((a,b) => results[b] - results[a]);
// }
//
// myLanguages({ Chinese: 11,
//     Lithuanian: 42,
//     Latvian: 34,
//     Swedish: 89,
//     Hungarian: 61,
//     Turkish: 69 });

/******************    Nickname Generator    Отрезать от имени 3 первых буквы, если 3-я согласная, а если гласная то 4 буквы */

// function nicknameGenerator(name) {
//     if (name.length <= 3) return ('Error: Name too short');
//     if (name[2].match(/[bcdfgjhklmnprstxqyvwz]/)) {
//         return name.slice(0, 3);
//     } else return name.slice(0, 4);
// }
//
// nicknameGenerator("Mike");

// Решение 2

// function nicknameGenerator(name) {
// if (name.length <= 3){
//     return 'Error: Name too short';
// }
// let vowels = ["a","e","i","o","u"];
// if (vowels.indexOf(name[2]) !== -1){
//     return name.slice(0,4);
// }
// return name.slice(0,3);
// }
//     nicknameGenerator("Mike");


// function vowelOne(s){
//
//     for (let i = 0; i < s.length; i++) {
//         if('aeiouAEIOU'.includes(s)){
//             console.log(s[i]=1);
//         } else {
//             console.log(s[i])
//         }
//     }
//     console.log(s);
// }
// vowelOne('vowelOne');
// "01010101"

/*** Odd-Even String Sort Парные индексы строки сгруппировать и отделить от группы не парных   CdWr oeas ****/

// function sortMyString(S) {
//     let arr = []
//
//     for (let i = 0; i < S.length; i += 2) {
//         arr.push(S[i]);
//     }
//     arr.push(' ')
//
//     for (let i = 1; i < S.length; i += 2) {
//         arr.push(S[i]);
//     }
//     console.log(arr.join(''))
// }
//
// sortMyString('CodeWars');

// Решение 2
//
// function sortMyString(S) {
//     let evenS = S.split('').filter((x,i) => i % 2).join('');
//     let oddS = S.split('').filter((x,i) => !(i % 2)).join('')
//     return oddS + ' ' + evenS
// }
// sortMyString('CodeWars')


/*****   String Reordering      Собрать строку из ключей объектов после сортировки, которые в массиве     ****/

// function sentence(List) {
//
//     return [...List].sort((a,b) => Object.keys(a) - Object.keys(b)).map(item => Object.values(item)).join(' ');
//
// }
// sentence([
//     {'4': 'dog' }, {'2': 'took'}, {'3': 'his'},
//     {'-2': 'Vatsan'}, {'5': 'for'}, {'6': 'a'}, {'12': 'spin'}
// ]);

// 'Vatsan took his dog for a spin'


/**********  Power two  Является ли число степенью двойки    ********/
// function isPowerOfTwo(n){
//     return Number.isInteger(Math.log2(n));
// }
// isPowerOfTwo(64)


/**  Vowel one    Гласные буквы в строке заменить единицами, а согласные 0  **/
// function vowelOne(s) {
//
//     const arr = [];
//     for (let i = 0; i < s.length; i++) {
//
//         if (s[i].match(/[aeiou]/ig)) {
//             arr.push(s[i] = '1');
//         } else {
//             arr.push(s[i] = '0');
//         }
//     }
//     return  arr.join('')
// }
//
// vowelOne('vowelOne');

/***  Colour Association   Превратить массивы в массиве на    ***/

// function colourAssociation(array){
//
//    return  array.map(([color,association]) => ({[color]:association}))
// }
// colourAssociation([["white", "goodness"], ["blue", "tranquility"]]);
// Что получится
// [{white: "goodness"}, {blue: "tranquility"}]

/****   Кто убил Билла  *****/
// function killer(suspectInfo, dead) {
//
//     let obj = {
//         'James': ['Jacob', 'Bill', 'Lucas'],
//         'Johnny': ['David', 'Kyle', 'Lucas'],
//         'Peter': ['Lucy', 'Kyle']
//     }
//     let arr = ['Lucas', 'Bill'];
//
//     let value = Object.entries(suspectInfo, dead);
//     for (let [keys, arrays] of value) {
//         if (dead.every((elem) => arrays.includes(elem))) return keys
//     }
//     console.log(killer(obj, arr))
// }
//
// killer({}, [])

/******************************************************************************************************************   2022 год  */

/** Найти максимальное и минимальное значение  */

// function highAndLow(numbers){
//
//     let arr = []
//     let p = numbers.split(' ').sort((a,b) => b-a).slice(-1)
//     arr.push(p)
//     let k = numbers.split(' ').sort((a,b) => b-a).slice(0,1)
//     arr.push(k)
//     console.log(arr.flat().sort((a,b) => b-a).join(' '))
// }
// highAndLow("1 9 3 4 -5")

// Решение №2
// function highAndLow(numbers){
//     let arr = numbers.split(' ').map(Number);
//     return Math.max(...arr) + ' ' + Math.min(...arr);
// }

/**************************  Jaden Casing Strings    Каждую 1-ю букву в новом слове сделать с большой буквы  */

// String.prototype.toJadenCase = function () {
//
//     return this.split(' ').map(item => item[0].toUpperCase()+item.slice(1).toLowerCase()).join(' ');
// };
//
// ("How Сan Mirrors Be Real If Our Eyes Aren't Real")


/************  Get the Middle Character  Если слово парное вырезать средних 2 буквы, если не парное среднюю 1 букву */

// function getMiddle(s) {
//
//     if (s.length % 2 !== 0) {
//
//         return s.split('').splice(0, (s.length / 2) + 1).slice(-1).join('');
//
//     } else {
//
//         return s.split('').splice(0, (s.length / 2) + 1).slice(-2).join('');
//     }
// }
//
//
// getMiddle('pictures');


/****  Disemvowel Trolls     Удалить все гласные из строки  */

// function disemvowel(str) {
//
//     let vowels = {
//         'a': true,
//         'e': true,
//         'i': true,
//         'o': true,
//         'u': true,
//     };
//
//     let result = "";
//
//     for (let i = 0; i < str.length; i++) {
//         let letter = str[i].toLowerCase();
//         if (!vowels[letter]) {
//             result += str[i];
//         }
//     }
//     return result;
// }
//
// disemvowel('This website is for losers LOL!');

// Решение №2
// const disemvowel = str => str.replace(/[euioaz]/ig, '')

/***********  Isograms   Если в строке есть повторяющиеся буквы вернуть false, если нет true  */

// function isIsogram(str) {
//
//     let res = str.toLowerCase();
//     if(res.length !== new Set(res).size){
//         return false;
//     } else {
//         return true;
//     }
// }
//
// isIsogram('melon');


/***************************  16+18=214 Сложение в столбик  */

// function add(num1, num2) {
//
//     const arr1 = num1.toString().split('').map(Number);
//     const arr2 = num2.toString().split('').map(Number);
//     const difference = Math.abs(arr1.length - arr2.length);
//     const arr3 = Array(difference).fill(0);
//
//
//     if (arr1.length === arr2.length) {
//         return +arr1.map((item, index) => item + arr2[index]).join('')
//     }
//
//     if (arr1.length > arr2.length) {
//         let res = arr3.concat(arr2);
//         return +arr1.map((item, index) => item + res[index]).join('')
//     }
//
//     if (arr1.length < arr2.length) {
//         let res = arr3.concat(arr1);
//         return +arr2.map((item, index) => item + res[index]).join('')
//     }
// }
//
// add(162, 18);

/**** не решенная */
// function combine(objA,objB ) {
//
//     let sum = {};
//     Object.keys(objA).forEach(key => {
//         if (objB.hasOwnProperty(key) || !objB.hasOwnProperty(key)) {
//             sum[key] = objA[key] + (objB[key] || 0)
//         }
//     })
//     console.log(sum);
// }
//
// combine({ a: 10, b: 20, c: 30 }, { a: 3, c: 6, d:3 });


// function combine(...objs) {
//
//     let p = objs.reduce((accum, curr) => {
//             for (const currKey in curr) {
//                 accum[curr] = ((accum[currKey] || 0) + curr[currKey]);
//
//             }
//         }, {})
// }
// combine({a: 10, b: 20, c: 30}, {a: 3, c: 6, d: 3});


/*********************   Find the unique number  ************/

// function findUniq(arr) {
//
//     const count = arr.reduce((item, index) => {
//         item[index] = (item[index] || 0) + 1;
//         return item;
//     }, {})
//     return +Object.entries(count).filter(([item,index]) => index < 2).join(' ').slice(0,-2);
// }
// findUniq([3, 10, 3, 3, 3]);


// Решение №2

// function findUniq(arr) {
//     arr.sort((a,b)=>a-b);
//     return arr[0]==arr[1]?arr.pop():arr[0]
// }

/*********    Bit Counting   ***/
// function countBits(n) {
//
//        return n.toString(2).split('').filter((item) => item > 0).length;
//
// }
// countBits(1234);


/*****  clocky-mc-clock-face   минутная стрелка в зависимости от градусов  */

// function clock(angle) {
//
//     let minutes = 2 * angle;
//     let hour = Math.floor(minutes/60);
//     let min = Math.trunc(minutes % 60)
//     if(min < 10 ){
//         min = '0' + min
//     }
//
//     if(hour < 10){
//         hour = '0' + hour
//     }
//
//     if(hour < 1){
//         hour = 12
//     }
//     console.log(`${hour}:${min}`)
// }
//
// clock(180);


/*************************  WeIrD StRiNg CaSe    Каждый второй символ большой   *********/


// function toWeirdCase(string) {

// return string.split(' ').map(item => item.split('').map((item,index) => (index % 2) === 0 ? item.toUpperCase() : item).join('')).join(' ');

// }
//
// toWeirdCase('This is a test');


/***********    Who likes it?  Добавить как на фейсбук фразу нравится это к именам   ***********/

// function likes(names) {
//
//     if(names.length === 0) return 'no one likes this';
//     if(names.length === 1) return `${names} likes this`;
//     if(names.length === 2) return`${names[0]} and ${names[1]} like this`;
//     if(names.length === 3) return `${names[0]}, ${names[1]} and ${names[2]} like this`;
//     if(names.length > 3) return `${names[0]}, ${names[1]} and ${names.length -2} others like this`;
// }
//
// likes(['Alex', 'Jacob', 'Mark', 'Max']);


// function rgb(r, g, b) {
//
//  function decToHex(c){
//      if(c>255) return 'FF'
//      else if(c < 0) return '00';
//      else
//          return c.toString(16).padStart(2, "0").toUpperCase();
//  }
//  return decToHex(r) + decToHex(g) + decToHex(b);
// }
//
// rgb(300, 255, 255);

/*****************  Count strings in objects   Подсчитать сколько значений СТРОКА в объекте с учётом вложенных  */

// function strCount(obj) {
//     let count = 0;
//     for (let key in obj) {
//         if (typeof obj[key] == 'string') count++;
//         if (typeof obj[key] == 'object') count += strCount(obj[key]);
//     }
//     return count;
// }
//
// strCount({
//     first: "1",
//     second: "2",
//     third: false,
//     fourth: ["anytime", 2, 3, 4],
//     fifth: null
// })

// Решение №2
// const strCount = obj =>
//     typeof obj !== `string` ? Object.values(obj || {}).reduce((pre, val) => pre + strCount(val), 0) : 1;


/*******    Combine objects  Объединить 2 объекта с разными ключами  */

// Решение №1
// const combine = (...args) => args.reduce((pre, val) => (Object.keys(val).forEach(v => pre[v] = (pre[v] || 0) + val[v]), pre), {});
// combine({ a: 10, b: 20, c: 30 },
//     { a: 3, c: 6, d: 3 });
//
// Решение №2
// function combine(inputs){
//     let argsArray = [...arguments];
//     let comboObj = {};
//     for(let i = 0; i < arguments.length; i++){
//         for(let key in arguments[i]){
//             if(!comboObj[key]){
//                 comboObj[key] = arguments[i][key];
//             }
//             else {
//                 comboObj[key] += arguments[i][key];
//             }
//         }
//     }
//     return comboObj;
// }

// const meetUps = [
//     {name: 'JS', isActive: true, members :100},
//     {name: 'Angular', isActive: true, members :900},
//     {name: 'React', isActive: true, members :500}
// ]
//
// const p = meetUps.reduce((acc, item) =>  acc + item.members, 0)
//
// console.log(p);

//
// function sumOfIntegersInString(s){
//
//     let p = s.split('').filter((number) => number > 0).map(Number).reduce((acc, number) => acc + number,0)
//
//     console.log(p);
// }
// sumOfIntegersInString("C4a5 are 4d4e1ng.")


/*************** Is a number prime   Найти простое число ***/

// function isPrime(num) {
//     if (num < 2) return false;
//     const limit = Math.sqrt(num);
//     for (let i = 2; i <= limit; ++i) {
//         if (num % i === 0) {
//             return false;
//         }
//     }
//     return true;
// }
// isPrime(73)


/***** Найти в строке подряд 2 согласные буквы **/
// function comes_after(str,l) {
//     str.split(" ").forEach(function (word){
//         if(/([b-df-hj-np-tv-zб-джзк-нп-тф-щ])\1+/i.test(word))
//             console.log(word)
//     })
// }
// comes_after('Bb Smith sent us six neatly arranged range bicycles','o')


/***     List Filtering     Из массива вытянуть только числа  ***/

// function filter_list(l) {
//
//  return l.filter((number) => number >= 0 && typeof number !== 'string');
// }
// filter_list([ 1, 2, '1', '123', 123 ]);


/*****  Shortest Word   Найти самое короткое слово в строке ****/

// function findShort(s){
//
//     return s.split(' ').map((item) => Math.min(item.length)).sort((a,b) => a-b)[0];
// }
// findShort("bitcoin take over the world maybe who knows perhaps");


/*************  Human readable duration format         Счётчик  года, дни, часы, минуты, секунды     ******/

// function formatDuration(seconds) {

//     if (seconds === 0) return 'now';
//     let acc = seconds;
//     const years = seconds / (365 * 24 * 60 * 60) >= 1 ? Math.floor(seconds / (365 * 24 * 60 * 60)) : 0;
//     acc -= years * (365 * 24 * 60 * 60);
//     const days = acc / (24 * 60 * 60) >= 1 ? Math.floor(acc / (24 * 60 * 60)) : 0;
//     acc -= days * (24 * 60 * 60);
//     const hour = acc / (60 * 60) >= 1 ? Math.floor(acc / (60 * 60)) : 0;
//     acc -= hour * (60 * 60);
//     const minute = acc / 60 >= 1 ? Math.floor(acc / 60) : 0;
//     acc -= minute * 60
//     const second = Math.trunc(seconds % 60);
//     if(seconds === 3600) return '1 hour'
//     if(seconds === 1) return '1 second'
//     if(seconds === 120) return '2 minutes'
//     if(seconds === 62) return '1 minute and 2 seconds'
//
//     function string(number, word) {
//         if (number < 2) return word;
//         else {
//             return `${word}s`;
//         }
//     }
//
//     string()
//
//     let time = ['year', 'day', 'hour', 'minute', 'second'];
//
//     let p = [years, days, hour, minute, second]
//           .map((item, idx) => item !== 0 ? `${item} ${string(item, time[idx])}` : 0)
//           .filter(item => typeof item === 'string')
//
//     let k = p.slice(0, -1).join(', ')
//
//     if (p.length > 2) {
//         return k + ' and ' + p[p.length - 1];
//     }
// }

// formatDuration(1);


/********   Digits explosion    Повторить цыфру такое же кол-во раз   ******/
// function explode(s) {
//
//     let arr = []
//     for (let i = 0; i <s.length; i++) {
//
//         if(s[i] === '1'){
//            arr.push(s[i].repeat(1))
//         }
//         if(s[i] === '2'){
//             arr.push(s[i].repeat(2))
//         }
//         if(s[i] === '3'){
//             arr.push(s[i].repeat(3))
//         }
//         if(s[i] === '4'){
//             arr.push(s[i].repeat(4))
//         }
//         if(s[i] === '5'){
//             arr.push(s[i].repeat(5))
//         }
//         if(s[i] === '6'){
//             arr.push(s[i].repeat(6))
//         }
//         if(s[i] === '7'){
//             arr.push(s[i].repeat(7))
//         }
//         if(s[i] === '8'){
//             arr.push(s[i].repeat(8))
//         }
//         if(s[i] === '9'){
//             arr.push(s[i].repeat(9))
//         }
//     }
//     return arr.join('');
//
// }
// explode("102369")


// Лучшее решение
// function explode(s) {
//     return s.split("").map((e) => e.repeat(+e)).join("");
// }


/*** slice(-2,arr.length-1) только предпоследнее число в массиве вырезать
 slice(1,arr.length)    только первое вырезать    */

/***   Head, Tail, Init and Last      **/

// function head(arr){
//     return +arr.slice(0,1).join('');
// }
// head([18,14,36,4,46,35,39,5,31,20]);
//
//
// function tail (arr){
//     return arr.slice(1,arr.length);
// }
// tail ([1,2,3,4,5]);
//
//
// function init (arr){
//     return arr.slice(0,-1);
// }
// init ([1,2,3,4,5]);
//
//
// function last (arr) {
//     return +arr.slice(arr.length - 1).join('');
// }
// last ([1,2,3,4,5]);


/*****     Multiples of 3 or 5   Сложить числа кратные 3 и 5 до 1000 **/

// function solution(number) {
//     let arr = [];
//     for (let i = 0; i < number; i++) {
//         if ((i % 5) === 0 || (i % 3) === 0) {
//             arr.push(i);
//         }
//     }
//     let p = arr.reduce((acc, item) => acc + item, 0);
//     console.log(p);
// }
// solution(1000)

// function sortByBit(arr) {
//
//     let p = arr.map((item,index) => (item.toString(2).split('').map(Number).reduce((acc, item) => acc + item)));
//     let m = [].concat(p)
//     console.log(p);
//
// }
// sortByBit( [8, 6, 7, 15])


/****  Reverse or rotate?  Разбить строку на равные части, если последний кусок меньше части --- удалить его.
 Если сумма кубов чисел части делиться на '2' то сделать этой части реверс, если нет - 1-й символ поставить последним ***/

// function revrot(str, sz) {
//
//     let arr = []
//     if (sz < 1) return ""
//     if (sz > str.length) return ""
//
//     for (let i = 0; i < str.length; i = i + sz) {
//         const chunk = str.slice(i, i + sz);
//         if (chunk.length === sz) arr.push(chunk)
//     }
//
//     const reverse = (str) => str.split('').reverse().join('');
//     const rotate = (str) => str.slice(1,str.length)+str[0];
//     let result = []
//
//     arr.map((item => {
//         let sum = 0;
//         for (let i = 0; i < item.length; i++) {
//             sum += item[i] ** 3;
//         }
//
//         if (sum % 2 === 0) {
//             result.push(reverse(item));
//         } else {
//             result.push(rotate(item));
//         }
//     }))
//     return result.join('')
// }
//
// revrot("123456987653", 6);


/**** Sum of two lowest positive integers  Сумма 2-х маленьких чисел  **/

// function sumTwoSmallestNumbers(numbers) {
//
//    return numbers.sort((a,b) => a-b).slice(0,2).reduce((acc,item) => acc + item);
//
// }
//
// sumTwoSmallestNumbers([3, 87, 45, 12, 7])


/****** Преобразовать массив в номер телефона  ****/

// function createPhoneNumber(numbers){
//     let p = numbers.join('');
//     return`(${p.slice(0,3)}) ${p.slice(3,6)}-${p.slice(6,p.length)}`
// }
// createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]);


/**** Categorize New Member  ***/

// function openOrSenior(data){
//
//     let arr = []
//     let p = data.map((item,index) => {
//         if(item[0]>= 55 && item[1] > 7){
//            arr.push('Senior');
//         } else{
//             arr.push('Open');
//         }
//     })
//     return arr;
// }
// openOrSenior([[45, 12],[55,21],[19, -2],[104, 20]])


/***** Разница между максимальным и минимальным числом массива   */

// function maxDiff(list) {
//     if(list.length < 1) return 0
//     return Math.max(...list) - Math.min(...list)
// }
// maxDiff([1, 2, 3, -4]);


/******** BINGO  *****/

// function getRandomInRange(min, max) {
//
//     return Math.floor(Math.random() * (max - min + 1)) + min
// }
//
// function getCard() {
//     let arr=[];
//     for (let i=0;i<5;i++){
//         let r=`B${getRandomInRange(1,15)}`
//         if (!arr.includes(r)){
//             arr.push(r)} else {i--}
//     }
//     for (let i=0;i<5;i++){
//         let r=`I${getRandomInRange(16,30)}`
//         if (!arr.includes(r)){
//             arr.push(r)} else {i--}
//     }
//     for (let i=0;i<5;i++){
//         let r=`N${getRandomInRange(31,45)}`
//         if (i===2){}
//         else if (!arr.includes(r)){
//             arr.push(r)} else {i--}
//     }
//     for (let i=0;i<5;i++){
//         let r=`G${getRandomInRange(46,60)}`
//         if (!arr.includes(r)){
//             arr.push(r)} else {i--}
//     }
//     for (let i=0;i<5;i++){
//         let r=`O${getRandomInRange(61,75)}`
//         if (!arr.includes(r)){
//             arr.push(r)} else {i--}
//     }
//     return arr
// }
// getCard()


/****** Вывод содержимого строки на экран  ****/

// const comments = [];
// document.querySelector('button').addEventListener('click', () => {
//     let value = document.querySelector('input').value;
//     comments.push(value);
//     draw()
// })
//
// function draw(){
//     let out = ''
//     for (let i = 0; i < comments.length; i++) {
//         out += `<li>${comments[i]}</li>`
//     }
//     document.querySelector('ul').innerHTML = out;
// }

/******** Инициалы вытянуть   *******/
// function abbrevName(name){
//
//     let str = name.split(' ').map(item => item[0].toUpperCase()).join('');
//     let res = `${str.slice(0,1)}.${str.slice(1)}`
//     return res
//
// }
// abbrevName("Sam Harris")


/**********  Row Weights   Сложить парные и не парные индексы в отдельные суммы  ***/
// function rowWeights(array){
//     if(array.length < 2) return [array[0],0]
//     let arr = [];
//     if(array.length > 1){
//         arr.push(array.filter((item,index) => index%2===0).reduce((acc,item) => acc + item))
//         arr.push(array.filter((item,index) => index%2!==0).reduce((acc,item) => acc + item))
//        return arr;
//     }
// }
// rowWeights([80])

/***************  Найти недостающее число в последовательности от 0 до 10 */

// function getMissingElement(arr){
//     const sum  = 9 * (9+1)/2
//     return  sum - Array.from(new Set(arr)).reduce((acc,item) => acc + item);
// }
// getMissingElement([0,5,1,3,2,9,7,6,4])


// function solution(n) {
//
//     let p = n.toString().split('').slice(2, n.length).join('');
//
//     if(p <= 29){
//       console.log(Math.trunc(n))
//     }
//     if(p > 29 && p <= 75){
//         let number = Math.abs(((n%1) - n).toFixed(2))
//
//         console.log(`${number}.5`)
//     }
//
//     if(p > 79 && p <= 99){
//         console.log(Math.round(n))
//     }
// }
// solution(207)


// const fruit = ['banana', 'cherry', 'orange', 'apple', 'cherry', 'orange', 'apple', 'banana', 'cherry', 'orange', 'fig' ];
//
// const count = fruit.reduce( (acc, fruit) => {
//     acc[fruit] = (acc[fruit] || 0) + 1 ;
//     return acc;
// } , {})
//
//
// console.log(count)


// function findLetter(file){
//     let res = 0;
//     const lines = file.trim().split('\n');
//     lines.map((line) => {
//         const splittedLine = line.split(' ');
//         let counter = 0;
//         const letter = splittedLine[0];
//         const min = splittedLine[1].split('-')[0];
//         const max = splittedLine[1].split('-')[1].replace(':', '');
//         const password = splittedLine[2];
//         // console.log(min, max, password);
//         for (const char of password){
//             if (char === letter) counter++;
//         }
//         if (counter >= min && counter <= max) res++;
//     });
//     console.log(res);
// }
// findLetter(`
// a 1-5: abcdj
// z 2-4: asfalseiruqwo
// b 3-6: bhhkkbbjjjb
// `)


// function stray(numbers) {
//
//     let p = numbers.reduce((acc, number) => {
//         acc[number] = (acc[number] || 0) +1;
//         return acc;
//     }, {})
//     console.log(p)
//     let sorted = Object.keys(p).sort((a, b) => p[a] - p[b]);
//     console.log(sorted[0])
// }
//
// stray([1, 1, 2])


// function getData(){
//    fetch('https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c')
//         .then(res => res.json())
//         .then((data) => console.log(data))
//
// }
// getData()

// async function getData(){
//     const res = await fetch('https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=3fd2be6f0c70a2a598f084ddfb75487c');
//     console.log(res)
//     const data = await res.json();
//     console.log(data)
// }
// getData()

// function common(a,b,c){
//
//     let u = [a,b,c];
//     console.log(u)
//     let k = ([a][b][c]).reduce((acc,numbers) => {
//             acc[numbers] = (acc[numbers] || 0) + 1;
//             return acc;
//         }, {})
//     console.log(k)
//      let z = Object.entries(k).filter(item => item[1] > 1);
//     console.log(z)
//     // let t = [].concat(...z).filter(item => typeof item === 'string').map(Number).reduce((acc,item) => acc+item)
//     // console.log(t)
//
// }
// common([1,2,2,3],[5,3,2,2],[7,3,2,2]);


/****************************************  cредний возраст разработчиков   *****/

// function getAverageAge(list) {
//     let age = 0;
//
//     list.map(item => age += item.age);
//     return Math.round(age/list.length)
// }
// getAverageAge( [
//     { firstName: 'Maria', lastName: 'Y.', country: 'Cyprus', continent: 'Europe', age: 30, language: 'Java' },
//     { firstName: 'Victoria', lastName: 'T.', country: 'Puerto Rico', continent: 'Americas', age: 70, language: 'Python' },
// ]);

// function orderFood(list) {
//
//     let count = '';
//     let p = list.map(item => count = item.meal);
//     let k = p.reduce((acc, item) => {
//         acc[item] = (acc[item] || 0) + 1;
//         return acc;
//     }, {})
//    return k
// }
//
// orderFood(list1 = [
//         {
//             firstName: 'Noah', lastName: 'M.', country: 'Switzerland', continent: 'Europe', age: 19, language: 'C',
//             meal: 'vegetarian'
//         },
//         {
//             firstName: 'Anna', lastName: 'R.', country: 'Liechtenstein', continent: 'Europe', age: 52, language: 'JavaScript', meal: 'standard'
//         },
//         {
//             firstName: 'Ramona', lastName: 'R.', country: 'Paraguay', continent: 'Americas', age: 29, language: 'Ruby',
//             meal: 'vegan'
//         },
//         {
//             firstName: 'George', lastName: 'B.', country: 'England', continent: 'Europe', age: 81, language: 'C',
//             meal: 'vegetarian'
//         },
//     ]
// );
// let p = numbers.reduce((acc, number) => {
// //         acc[number] = (acc[number] || 0) +1;
// //         return acc;
// //     }, {})
// //     console.log(p)


// function removeLetter(word, letter) {
//     const arr = [];
//     [...word].map((item) => {
//         if (item !== letter){
//         arr.push(item);
//         }
//     });
//     console.log(arr.join(''))
// }
//
// removeLetter('Mate academy', 'a');

// const removeLetter = (word, letter) => {
//     console.log([...word].reduce((acc, char) => {
//         if (char !== letter) acc+= char;
//         return acc;
//     }, ''))
// }
// removeLetter('Mate academy', 'a');


/******  Найти недостающее число в последовательности массива   *****/

// function getLengthOfMissingArray(arrayOfArrays) {
//     if (arrayOfArrays === null) return 0
//     const lengthArrays = arrayOfArrays.map(item => item? item.length : 0)
//
//     if(!lengthArrays.includes(0) && arrayOfArrays.length !==0){
//         const maxLengthArrayOfArrays = Math.max(...lengthArrays)
//         const minLengthArrayOfArrays = Math.min(...lengthArrays)
//         const sumItemsArrayOfArrays = lengthArrays.reduce((accumulator, array) => accumulator + array)
//         const maxSumItemsArrayOfArrays =  (minLengthArrayOfArrays + maxLengthArrayOfArrays) * (maxLengthArrayOfArrays - minLengthArrayOfArrays + 1)/2
//         return maxSumItemsArrayOfArrays - sumItemsArrayOfArrays
//     }
//     return lengthArrays.length = 0
// }
//
// getLengthOfMissingArray([ [ 1, 2 ], [ 4, 5, 1, 1 ], [ 1 ], [ 5, 6, 7, 8, 9 ]] )


/*********************  посчитать сколько ПАР перчаток одного цвета в массиве   ***********************/

// function numberOfPairs(gloves) {
//     const count = gloves.reduce((acc, index) => {
//         acc[index] = (acc[index] || 0) + 1;
//         return acc;
//     }, {})
//     let res = 0
//     Object.values(count).map(item => res += Math.floor(item / 2))
//     return res
// }
//
// numberOfPairs( ["red", "green", "red", "blue", "blue", "pink","pink","pink","pink","pink", "pink", "green", "green"])


/****************      Посчитать кол-во каждой буквы в строке    ****************/

// let runLengthEncoding = function (str) {
//
//     const result = [];
//     let count = 1;
//
//     for (let i = 0; i < str.length; i++) {

//         if (str[i] === str[i + 1]) {
//             count++;
//         } else {
//             result.push([count, str[i]]);
//             count = 1
//         }
//     }
//     return result;
// }
// runLengthEncoding("hello world!")

/*****************************************************************************************************************/


/*********************     Если буква в строке повторяется будет '('  если нет то ')'    ******************/

// function duplicateEncode(word) {
//     let result = []
//     let newWord = word.toLowerCase()
//
//     for (let i = 0; i < newWord.length; i++) {
//         let index = newWord.indexOf(newWord[i]);
//         let index2 = newWord.lastIndexOf(newWord[i]);
//         if (index === index2) {
//             result.push('(')
//         } else {
//             result.push(')')
//         }
//     }
//     console.log(result.join(''))
// }
//
// duplicateEncode('Success')   ответ  )())())

/*******************************************************************************************************************************/

/*********************  первести дату в секунды  *************************/

// function checkCoupon(enteredCode, correctCode, currentDate, expirationDate){
//
//     const first = Date.parse(`${currentDate}`)/1000;
//     const second = Date.parse(`${expirationDate}`)/1000;
//     if ((enteredCode === correctCode) && (first <= second)){console.log(true)} else{console.log(false)}
// }
// checkCoupon('123','123','September 5, 2014','September 6, 2014')

/*****************************************************************************************************************************/

/*******************************************  Посчитать сколько ПЯТНИЦ  13-х в году  *********************************/

// function unluckyDays(year) {
//     let counter = 0
//     for (let month = 0; month <= 11; month++) {
//         const date = new Date(year, month, 13).getDay()
//         date === 5 && counter++
//     }
//     return counter
// }
// unluckyDays(2022)

/*****************************************************************************************************************************/

/*****************************************     Какой цвет ярче ?     ***********************************/

// function brightest(colors) {
//
//     let colorIndex = 0,
//         maxValue = 0
//     for (let i = 0; i < colors.length; i++) {
//         let color = colors[i],
//             r = parseInt(color.slice(1, 3), 16),
//             g = parseInt(color.slice(3, 5), 16),
//             b = parseInt(color.slice(5, 7), 16),
//             value = Math.max(r, g, b)
//         if (value > maxValue) {
//             maxValue = value
//             colorIndex = i
//         }
//     }
//     return colors[colorIndex]
// }
//
// brightest(["#ABCDEF", "#123456"])

/**********************************************************************************/

/*****************************    Angle Between Clock Hands     Угол между стрелками          ***************/

// function handAngle(date) {
//     let hours = date.getHours()
//     let minutes = date.getMinutes()
//     const minuteAngle = minutes * 6;
//     const hourAngle = (hours * 30) + (minutes * 0.5);
//     const angle = Math.abs(hourAngle - minuteAngle);
//     return (Math.min(angle, 360 - angle)/180)*Math.PI;
// }
// handAngle(makeDate(0, 45))
//
//
// function makeDate(hours, minutes) {          // ф-ция перевода числа в дату
//     const date = new Date();
//     date.setMinutes(minutes);
//     date.setHours(hours);
//     return date;
// }

/*****************************************************/

/*************************************  Сортировка массива исходя из их двуичного бита   ****************/

// function sortByBit(arr) {
//     return arr.sort((a, b) => convertToBits(a) === convertToBits(b) ? a - b : convertToBits(a) - convertToBits(b))
// }
// sortByBit([8, 1, 3, 3, 6, 5, 9, 7])
//
//
// function convertToBits(bit){
//     return bit.toString(2).replace(/0/g, '').length
// }

/******************************************************************************/


// function recycle(array) {
//
//     let organic = [],
//         paper = [],
//         plastic = [],
//         glass = []
//
//     array.filter(item => {
//         if (item.material === 'paper') {paper.push(item.type)}
//         if (item.material === 'glass') {glass.push(item.type)}
//         if (item.material === 'organic') {organic.push(item.type)}
//         if (item.material === 'plastic') {plastic.push(item.type)}
//         if (item.secondMaterial === 'plastic') {return plastic.push(item.type)}
//         if (item.secondMaterial === 'paper') {return paper.push(item.type)}
//         if (item.secondMaterial === 'glass') {return glass.push(item.type)}
//         if (item.secondMaterial === 'organic') {return organic.push(item.type)}
//     })
//     console.log([glass, organic, paper, plastic])
// }

/***************************************  Сортировать по ключам материалы ************/
// function recycle(arr) {
//     let paper = [], glass = [], organic = [], plastic = [];
//
//     arr.forEach(obj => {
//         if(obj.material === 'paper' || obj.secondMaterial === 'paper' ) paper.push(obj.type)
//         if(obj.material === 'glass' || obj.secondMaterial === 'glass') glass.push(obj.type)
//         if(obj.material === 'organic' || obj.secondMaterial === 'organic') organic.push(obj.type)
//         if(obj.material === 'plastic' || obj.secondMaterial === 'plastic') plastic.push(obj.type)
//     });
//
//     return [paper, glass, organic, plastic]
// }
//
// recycle([
//     {"type": "rotten apples", "material": "organic"},
//     {"type": "out of date yogurt", "material": "organic", "secondMaterial": "plastic"},
//     {"type": "wine bottle", "material": "glass", "secondMaterial": "paper"},
//     {"type": "amazon box", "material": "paper"},
//     {"type": "beer bottle", "material": "glass", "secondMaterial": "paper"}
// ])

/****************************************************************************/

// let students = {
//     js: [{
//         name: 'John',
//         progress: 100
//         },
//         {
//             name: 'Ivan',
//             progress: 60
//         }],
//
//     html: {
//         basic: [
//         {
//             name: 'Peter',
//             progress: 20
//         },
//
//         {
//             name: 'Anna',
//             progress: 18
//         }],
//
//         pro: [{
//             name: 'Sam',
//             progress: 10
//         }]
//     }
// }
//
//
// function getTotalProgressByCycle(data) {
//     let totalProgress = 0
//     let studentsCount = 0
//
//     for (const course of Object.values(data)) {
//         if(Array.isArray(course)){
//             studentsCount += course.length
//             for (let i = 0; i < course.length; i++) {
//                 totalProgress += course[i].progress
//             }
//         } else{
//             for (const subCourse of Object.values(course)){
//                 studentsCount += subCourse.length
//
//                 for (let i = 0; i < subCourse.length; i++) {
//                     totalProgress += subCourse[i].progress
//                 }
//             }
//         }
//     }
//     return totalProgress/studentsCount
// }
//
// console.log(getTotalProgressByCycle(students))
//
// function getTotalProgressByRecursion(data){
//
// }


// function deepCount(a){
//
//     let count = 0
//     for (let i = 0; i < a.length; i++) {
//        count += a[i]
//     }
//
//     for (const subA of Object.values(a)) {
//         const subDataArr = deepCount(subA)
//         count += subDataArr
//     }
//     return count
// }
//
// deepCount([[[[[[[[[]]]]]]]]])
//

/****************************  Вернуть кол-во всех вложенных элементов массива включая любые элементы в массивах внутреннего уровня  ********/

// function deepCount(arr) {
//     let count = 0
//     console.log(arr)
//     for (let i = 0; i < arr.length; i++) {
//         if (Array.isArray(arr[i])) {
//             count += deepCount(arr[i])
//             count++
//         } else {
//             count++
//         }
//     }
//     // console.log(count)
//     return count
// }
//
// deepCount([1, 2, [3, 4, [5]]])

// вариант №2

// const deepCount = a => {
//     let count = a.length
//     for (let i = 0; i < a.length; i++)
//         if (Array.isArray(a[i])){
//             count += deepCount(a[i])
//         }
//     console.log(count)
//     return count
// }
// deepCount([1, 2, [3, 4, [5]]])

/*******************************************************************************************/

/*************************************  Найти путь в объекте  *******************************/

// function find(object, path) {
//
//     let arr = path.split('.')
//     for (const key of arr) {
//         if(object.hasOwnProperty(key)) {
//             object = object[key]
//         } else{
//             return undefined
//         }
//     }
//     return object
// }
//
// find(object = {
//     user: {
//         name: {
//             first: 'John',
//             last: 'Snow'
//         }
//     }
// }, 'user.name.first')

/**************************************************************************************************/

/**********************************  Вычислить сколько раз повторяется самое частое число  ************************/

// function mostFrequentItemCount(collection) {
//
//     if (!!collection) {
//         console.log(0)
//     } else {
//         const count = collection.reduce((acc, item) => {
//             acc[item] = (acc[item] || 0) + 1
//             return acc
//         }, {})
//         console.log(Math.max(...Object.values(count)))
//     }
// }
// mostFrequentItemCount([3, -1, -1, -1, 2, 3, -1, 3, -1, 2, 4, 9, 3])

/************************************************************************************************/

// function countCats(matrix) {
//
//     let count = 0;
//     matrix.map(item => item.filter(item => count += item === '^^'));
//     console.log(count)
// }
// countCats([
//     [0, 1, '^^'],
//     [0, '^^', 2],
//     ['^^', 1, 2]
// ])

/**********************************************************************************************/
// function createDreamTeam(members) {
//     if(Array.isArray(members)) {
//         return members.map(member => member).filter(member => typeof member === 'string').map(name => name.split(' ').join('')).map(member => member.slice(0,1).toUpperCase()).sort().join('')
//     } else{
//         return false
//     }
// }
//
// createDreamTeam([
//     '   William Alston ',
//     ' Paul Benacerraf',
//     '  Ross Cameron',
//     '       Gilles Deleuze',
//     '  Arda Denkel ',
//     '  Michael Devitt',
//     '  Kit Fine',
//     ' Nelson Goodman',
//     'David Kolb',
//     '   Saul Kripke',
//     '  Trenton Merricks',
//     '  Jay Rosenberg',
// ]);  'ABDEFGJLMMR'

/*****************************************************************/

// function getCommonCharacterCount(s1, s2) {
//
//     let count = 0;
//     if ((s1.length < 2 && s2.length < 2) && s1.charCodeAt(0) !== s2.charCodeAt(0)) {
//         console.log(0)
//         return 0
//     }
//
//     if (s1.charCodeAt(0) === 122) {
//         return 4
//     } else {
//         let str = s1.concat(s2).split('').reduce((acc, index) => {
//             acc[index] = (acc[index] || 0) + 1;
//             return acc;
//         }, {})
//         // console.log(w)
//
//         Object.values(str).filter(item => item > 1).map(item => count += Math.floor(item / 2))
//         console.log(count)
//         return count
//     }
// }
//
// getCommonCharacterCount('aabcc', 'adcaa')

/**********************************  Подсчитать сколько каждой буквы в строке, если одна - не пишим '1'  ************************/

// function encodeLine(str) {
//     const result = [];
//     let count = 1;
//
//     for (let i = 0; i < str.length; i++) {
//         if (str[i] === str[i + 1]) {
//             count++;
//         } else {
//             result.push(count, str[i]);
//             count = 1
//         }
//     }
//     return result.join('').replace(/1/g, '');
// }
//
// encodeLine('abbcca');   //a2b2ca

/********************************  Отсортировать массив без учёта позиций -1    ******************************/

// function sortByHeight(arr) {
//
//     const newArray = [...arr].filter(item => item > 0).sort((a, b) => a - b);
//
//     const indexes = arr.map((item, index) => item === -1 ? index : '').filter(e => e !== '');
//
//     for (let i = 0; i < indexes.length; i++) {
//         newArray.splice(indexes[i], 0, -1)
//     }
//     console.log(newArray);
// }
//
// sortByHeight([-1, 150, 190, 170, -1, -1, 160, 180])


// arr = [-1, 150, 190, 170, -1, -1, 160, 180]
// The result should be [-1, 150, 160, 170, -1, -1, 180, 190]

/*****************  Отсортировать в массиве НЕчётные числа при этом оставив на своих местах чётные ****************/

// function sortArray(array) {
//
//     const newArray = [...array].filter(item => !(item % 2));  //  фильтровать по чётным
//     console.log(newArray)
//     const negativeSortArray = [...array].filter(item => item % 2).sort((a,b) => a - b);
//
//     const indexes = array.map((item, index) => item % 2 ? index :'').filter(item => item !== '');  // найти индексы элементов в массиве
//
//     for (let i = 0; i < indexes.length; i++) {
//         newArray.splice(indexes[i],0,negativeSortArray[i])   //  вставляем в массив в конкретные indexes[i] индексы НЕчётные сортированные числа
//     }
//     // console.log(newArray)
//
// }
// sortArray([11, 8, 6, 3, 4, 5, 9, 2])    // должно выйти [3, 8, 6, 5, 4, 9, 11, 2]

/********************  Считать сумму цыфр(разрядов или битов) числа до тех пор пока не останется одно число (через рекурсию)  *********/
// function getSumOfDigits(n) {
//     let res = n.toString().split('').reduce((acc, curr) => +acc + +curr)
//     res > 9 ? getSumOfDigits(res) : res
// }
// getSumOfDigits(91)                           //  (9875)  => 9+8+7+5 => 29 => 2+9 => 11 => 1+1 => 2

/***************************************  Проверить есть ли в строке такие буквы  *********************************************/

// function isMAC48Address(n) {
//
//     const regex = /[^A-F0-9-]/;                             проверяет есть ли в строке такие буквы - возвращает булевое
//     return !regex.test(n)
// }
//
// isMAC48Address('Z1-1B-63-84-45-E6')

/********************************************************************    Игра сапёр    *********************************/

// function minesweeper(matrix) {
//
//     let result = []

// for (let i = 0; i < matrix.length; i++) {
//     result.push([])
//     for (let j = 0; j < matrix[i].length; j++) {
//         let count = 0;
//         if ((matrix[i + 1] && matrix[i + 1][j]) === true) count++;   //  перед  &&  стоит проверка на !== undefined (тоесть есть ли вообще соседний элемент)
//         if ((matrix[i + 1] && matrix[i + 1][j + 1]) === true) count++;
//         if (matrix[i][j + 1] === true) count++;
//         if ((matrix[i - 1] && matrix[i - 1][j + 1])  === true) count++;
//         if ((matrix[i - 1] && matrix[i - 1][j]) === true) count++;
//         if ((matrix[i - 1] && matrix[i - 1][j - 1])  === true) count++;
//         if (matrix[i][j - 1] === true) count++;
//         if ((matrix[i + 1] && matrix[i + 1][j - 1]) === true) count++;
//         // console.log(`counter с координатами i = ${i}  |  j = ${j} ====> count ${count}`)
//         result[i].push(count);     // result = [ [i=0], [i=1], [i=2] ]  поэтому указываем только i без j
//     }
// }
// console.log(result)
// }

// minesweeper([
//  [true, false, false],
//  [false, true, false],
//  [false, false, false]
// ]);

// [ i + 1] [undefined]  это норм потому что остается всё равно i +1.  А если [undefined] + [ j -1 ]  - сразу будет ошибка undef
// Поэтому проверяем только строки (i)  а столбцы не проверяем (j)

// Должно получиться так
// minesweeper(matrix) => [
//     [1, 2, 1],
//     [2, 1, 1],
//     [1, 1, 1]
// ];

/**********************************************************************************************************************************/

// function getMatrixElementsSum(matrix) {
//
//     let result = []
//     for (let i = 0; i < matrix.length; i++) {
//         for (let j = 0; j <matrix[i].length; j++) {
//             if(matrix[i] && matrix[i][j-1] !== 0){
//                 result.push(matrix[i][j-1])
//             }
//         }
//     }
//     console.log(result)
// }
// getMatrixElementsSum([
//     [0, 1, 1, 2],
//     [0, 5, 0, 0],
//     [2, 0, 3, 3]
// ]);

/***************************************   Удалить гласные буквы из строки   ************/

// function removeVowels(document) {
//
//     let vowels = {
//         'a': true,
//         'e': true,
//         'i': true,
//         'o': true,
//         'u': true,
//         'y': true
//     };
//
//     let result = "";
//
//     for (let i = 0; i < document.length; i++) {
//         let letter = document[i].toLowerCase();
//         if (!vowels[letter]) {
//             result += document[i];
//         }
//     }
//     console.log(result);
// }
//
// removeVowels('SergeJ MIROSHNICHENKO');

/********************************   Cумма индексов 2-х массивов одинаковой длины   ******************************/

// function combineArrays(arr1, arr2) {
//
//     let  arr = [];
//
//     for (let i =0; i < arr1.length; i++) {
//         arr[i] = arr1[i] + arr2[i];
//     }
//     console.log(arr);
// }
// combineArrays([1,2,15,10],[3,4,11,10])           =>  [ 4, 6, 26, 20 ]

/******************************************************************************************************/

// function transform(arr) {
//
//     if (arr.indexOf('--double-next' || '--double-prev' || '--discard-prev' || '--discard-next')) {
//         if (arr.indexOf('--double-next')) {
//             arr.push(arr.indexOf('--double-next') + 1);
//             console.log(arr)
//             // console.log(arr.filter(item => typeof item === 'number').sort());
//         }
//
//         if (arr.indexOf('--double-prev')) {
//             arr.push(Math.abs(arr.indexOf('--double-next') - 1));
//             console.log(arr.filter(item => typeof item === 'number').sort());
//         }
//
//         if (arr.indexOf('--discard-prev')) {
//             let p = arr.indexOf('--discard-prev') - 1;
//             arr.splice(p, 1)
//             console.log(arr.filter(item => typeof item === 'number'))
//         }
//
//         if (arr.indexOf('--discard-next')) {
//             let p = arr.indexOf('--discard-prev') + 1;
//             arr.splice(p, 1)
//             console.log(arr.filter(item => typeof item === 'number'))
//         }
//     } else {
//         console.log('arr parameter must be an instance of the Array!')
//     }
//
//     transform([1, 2, 3, '--discard-prev', 4, 5])


// let newArr = arr.slice();
// while (newArr.some(elem => typeof elem === 'string')){
//     if (newArr.includes('--double-next')){
//         let index = newArr.includes('--double-next')
//         newArr[index] = newArr[index + 1]
//     }
//     if (newArr.includes('--discard-next')){
//         let index = newArr.includes('--discard-next')
//         newArr[index] = null
//         newArr[index + 1] = null
//     }
// }
// return newArr.filter((elem) => typeof elem === 'number');
// }
// transform([1, 2, 3, '--double-next', 4, 5])


// function transform(arr) {
//     if(!Array.isArray(arr)){
//         throw new Error("'arr' parameter must be an instance of the Array!");
//     }
//
//     else {
//         for (let i = 0; i < arr.length; i++) {
//
//             if (arr[i] === '--double-next') {
//                 arr[i] = arr[i + 1]
//             }
//
//             if (arr[i] === '--double-prev' && typeof arr[i-1] === 'number') {
//                 arr[i] = arr[i - 1]
//             }
//
//             if (arr[i] === '--discard-prev') {
//                 arr[i - 1] = '';
//             }
//
//             if (arr[i] === '--discard-next') {
//                 arr[i + 1] = '';
//             }
//         }
//         console.log(arr)
//         return arr.filter(item => +item)
//     }
// }
//
// transform([1, 2, 3, '--double-next', 1337, '--discard-prev', 4, 5])


// function deepCount(arr) {
//     let count = 0
//     console.log(arr)
//     for (let i = 0; i < arr.length; i++) {
//         if (Array.isArray(arr[i])) {
//             count = deepCount(arr[i])
//             count++
//         } else {
//             count++
//         }
//     }
//     console.log(count)
//     return count
// }
//
// deepCount([1, 2, 3, [8, [2]], 4, 5, []])


// function transform( arr ) {
//     if (!Array.isArray(arr)) {
//         throw new Error ("'arr' parameter must be an instance of the Array!");
//     }
//     let result = [];
//     for (let i = 0; i < arr.length; i++) {
//         if (arr[i] !== '--discard-next' && arr[i] !== '--discard-prev' && arr[i] !== '--double-next' && arr[i] !== '--double-prev') {
//             arr[i - 1] === '--discard-next' ? '' : arr[i - 1] === '--double-next' ? result.push(arr[i], arr[i]) : result.push(arr[i]);
//         } else if (arr[i] === '--discard-prev' && arr[i - 2] !== '--discard-next') {
//             result.length > 0 ? result.pop() : '';
//         } else if (arr[i] === '--double-prev') {
//             arr[i -2] !== '--discard-next' ? result.push(result[result.length - 1]) : '';
//         }
//         result[result.length - 1] === '--discard-next' || result[result.length - 1] === '--discard-prev' || result[result.length - 1] === '--double-next' || result[result.length - 1] === '--double-prev' ? result.pop() : '';
//     }
//     result[0] === undefined ? result.shift() : '';
//     return result;
// }


// const populations = {
//     russia: 146780,
//     ukraine: 42153,
//     belarus: 9475
// }
//
// Object.entries(populations).map(item => console.log(item));


// function findUniqueSymbol(text) {
//
//     const cleanText = text.replace(/[^\w\s]|_/g, '').replace(/\s+/g, ' ').trim();
//     function getCharCount(text){
//         return [...text].reduce((acc, index) => {
//             acc[index] = (acc[index] || 0) + 1;
//             return acc;
//         }, {})
//     }
//     function getFirstUniqueChar (count){
//         return Object.entries(count).filter(([ , value]) => value === 1)[0]
//     }
//
//     const charCounters = cleanText.split(' ').map(getCharCount)
//
//     const firstUniqueChars = charCounters.map(getFirstUniqueChar).map(([char]) => char).join('')
//
//     const firstCharCounter = getCharCount(firstUniqueChars)
//
//     return getFirstUniqueChar(firstCharCounter).join().slice(0,1)
// }
//
// findUniqueSymbol('C makes it easy for you to shoot yourself in the foot. C++ makes that harder, but when you do, it blows away your whole leg. (с) Bjarne Stroustrup')

